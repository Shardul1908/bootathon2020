//setting canvas
var canvas : HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context : CanvasRenderingContext2D = canvas.getContext("2d");


//draw traingle
context.beginPath();
context.moveTo(250,300);
context.lineTo(300,400);
context.lineTo(200,400);
context.lineTo(250,300);
context.fillStyle = "violet"
context.fill();
context.strokeStyle = "red";
context.stroke();
//save the current canvas context
context.save();
//mirroring the above traingle
context.scale(1,-1);
context.translate(0,-600);

context.beginPath();
context.moveTo(250,300);
context.lineTo(300,400);
context.lineTo(200,400);
context.lineTo(250,300);
context.fillStyle = "green"
context.fill();
context.strokeStyle = "red";
context.stroke();

//restore
context.restore();

//right side traingle
context.beginPath();
context.moveTo(250,300);
context.lineTo(300,400);
context.lineTo(350,300);
context.lineTo(250,300);
context.fillStyle = "indigo"
context.fill();
context.strokeStyle = "red";
context.stroke();

//save
context.save();

//mirrorring
context.scale(1,-1);
context.translate(0,-600);

context.beginPath();
context.moveTo(250,300);
context.lineTo(300,400);
context.lineTo(350,300);
context.lineTo(250,300);
context.fillStyle = "blue"
context.fill();
context.strokeStyle = "red";
context.stroke();

//restore
context.restore();

//left side triangle
context.beginPath();
context.moveTo(250,300);
context.lineTo(200,400);
context.lineTo(150,300);
context.lineTo(250,300);
context.fillStyle = "orange"
context.fill();
context.strokeStyle = "red";
context.stroke();

//save
context.save();

//mirroring
context.scale(1,-1);
context.translate(0,-600);

context.beginPath();
context.moveTo(250,300);
context.lineTo(200,400);
context.lineTo(150,300);
context.lineTo(250,300);
context.fillStyle = "yellow"
context.fill();
context.strokeStyle = "red";
context.stroke();

//restore
context.restore();

