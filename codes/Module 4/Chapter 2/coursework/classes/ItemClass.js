class Item {
    constructor(id, quantity, name, price) {
        this._id = id;
        this._quantity = quantity;
        this.price = price;
        this.name = name;
    }
    display() {
        console.log("Name = " + biscuits.name);
        console.log("Price = " + biscuits.price);
    }
}
//object
var biscuits = new Item(1, 100, "Oreo", 30);
var grains = new Item(2, 200, "Wheat", 20);
biscuits.display();
grains.display();
//# sourceMappingURL=ItemClass.js.map