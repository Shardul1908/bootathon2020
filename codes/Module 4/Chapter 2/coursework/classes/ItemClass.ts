class Item {                    //Class Item    
    //Attributes
    private _id : number;
    private _quantity : number;
    name : string;
    price : number;

    constructor(id: number,quantity : number,name : string,price : number) {
        this._id = id;
        this._quantity = quantity;
        this.price = price;
        this.name = name;
    }

    display () {
        console.log("Name = " + biscuits.name);
        console.log("Price = " + biscuits.price);
    }
}
//object
var biscuits : Item = new Item(1,100,"Oreo",30);
var grains : Item = new Item(2,200,"Wheat",20);

biscuits.display();
grains.display();
