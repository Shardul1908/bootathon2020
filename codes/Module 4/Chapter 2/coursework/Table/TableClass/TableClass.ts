/*  Master Class Table
    Has tableElement , rows , Columns , Cells, Id
    Has function to create the Table with rows with generic cellDataType , id
    and to clear table
*/
class Table {
    private table : HTMLTableElement;
    protected rows : number;
    protected columns : number;
    private cellDataType : string;
    private id : string;
    
    constructor(table : HTMLTableElement,
                rows : number,
                columns : number,
                cellDataType : string,
                id : string) {
                    this.table = table;
                    this.rows = rows;
                    this.columns = rows;
                    this.cellDataType = cellDataType;
                    this.id = id;
                }

    protected createTable() {
        for(let i = 0;i<this.rows;i++) {
            var row : HTMLTableRowElement = this.table.insertRow();
            for(let j =0;j<this.columns;j++) {
                var cell : HTMLTableDataCellElement = row.insertCell();
                var type = document.createElement(this.cellDataType);
                type.id = this.id + i.toString() + j.toString();
                cell.appendChild(type);
            }   
        }
    }

    protected clearTable() {
        while(this.table.rows.length  > 0){
            this.table.deleteRow(0);
        } 
    }
}

class LableTable extends Table {
    
    constructor(table : HTMLTableElement , rows : number , columns : number,id : string) {
        super(table,rows,columns,"label",id);
    }

    create() {
        this.clearTable();
        this.createTable();
    }

    setText(id:string) {
        for(let i = 0;i<this.rows;i++) {
            for(let j =0;j<this.columns;j++) {
                var label : HTMLLabelElement = <HTMLLabelElement>document.getElementById(id + i.toString() + j.toString());
                label.innerHTML = i + " " + j;
            }   
        }
    }
}

class InputTable extends Table {
    private type : string;
    private _id : string;
    constructor(table : HTMLTableElement , rows : number , columns : number,id : string,type:string) {
        super(table,rows,columns,"input",id);
        this.type = type;
        this._id = id;
    }

    setType() {
        for(let i = 0;i<this.rows;i++) {
            for(let j =0;j<this.columns;j++) {
                var element : HTMLInputElement = <HTMLInputElement>document.getElementById(this._id + i.toString() + j.toString());
                element.type = this.type;

            }   
        }       
    }

    create() {
        this.clearTable();
        this.createTable();
    }

}

class TextInputTable extends InputTable {
    
    constructor(table : HTMLTableElement,rows : number , columns : number,id :string) {
        super(table,rows,columns,id,"text");
    }

    create(){
        super.create();
        super.setType();
    } 

    setText(id:string) {
        for(let i = 0;i<this.rows;i++) {
            for(let j =0;j<this.columns;j++) {
                var input : HTMLInputElement = <HTMLInputElement>document.getElementById(id + i.toString() + j.toString());
                input.placeholder = i + " " + j;
                input.className = "form-control-plaintext";
            }   
        }
    }
}

class ButtonInputTable extends InputTable {
    
    constructor(table : HTMLTableElement,rows : number , columns : number,id :string) {
        super(table,rows,columns,id,"button");
    }

    create(){
        super.create();
        super.setType();
    } 

    setText(id:string) {
        for(let i = 0;i<this.rows;i++) {
            for(let j =0;j<this.columns;j++) {
                var input : HTMLInputElement = <HTMLInputElement>document.getElementById(id + i.toString() + j.toString());
                input.value = i + " " + j;
            }   
        }
    }
}

 