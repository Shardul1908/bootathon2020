//number of row and columns for both the matrices
var row1;
var col1;
var row2;
var col2;
var mat1 = [];
var mat2 = [];
var mat3 = [];
//gets data from the user and multiplies the matrices 
function Multiply() {
    mat1 = LoadMatrix(row1, col1, "a");
    mat2 = LoadMatrix(row2, col2, "b");
    mat3 = mult(mat1, mat2, row1, col1, row2, col2);
    printResult();
}
//add function
function Add() {
    mat1 = LoadMatrix(row1, col1, "a");
    mat2 = LoadMatrix(row2, col2, "b");
    mat3 = add(mat1, mat2, row1, col1, row2, col2);
    printResult();
}
//substract functions
function Substract() {
    mat1 = LoadMatrix(row1, col1, "a");
    mat2 = LoadMatrix(row2, col2, "b");
    mat3 = sunstract(mat1, mat2, row1, col1, row2, col2);
    printResult();
}
//Populating Select Tags using funtions 
var select = document.getElementById("Row1");
addSelect(select);
select = document.getElementById("Col1");
addSelect(select);
select = document.getElementById("Row2");
addSelect(select);
select = document.getElementById("Col2");
addSelect(select);
//function to populate select tag passed as parameter
function addSelect(sel) {
    for (let i = 1; i <= 10; i++) {
        let option = document.createElement("option");
        option.text = i.toString();
        option.value = i.toString();
        sel.add(option);
    }
}
//Function for creating tables for the matrix.This is called by getMatrix1() getMatrix2()
function getMatrix(str, str1, id, row, col) {
    var p1 = document.getElementById(str1);
    p1.innerHTML = "Enter the Matrix : <br>";
    var tab = document.getElementById(str);
    var inputTable = new TextInputTable(tab, row, col, id);
    inputTable.create();
    inputTable.setText(id);
}
//onclick function for button Add matrix 1
function getMatrix1() {
    row1 = getSelectData("Row1");
    col1 = getSelectData("Col1");
    console.log(row1);
    console.log(col1);
    getMatrix("tb1", "p1", "a", row1, col1);
}
//onclick function for button Add matrix 2
function getMatrix2() {
    row2 = getSelectData("Row2");
    col2 = getSelectData("Col2");
    console.log(row2);
    console.log(col2);
    getMatrix("tb2", "p2", "b", row2, col2);
    var but = document.getElementById("b1");
    but.style.display = "";
    but = document.getElementById("b2");
    but.style.display = "";
    but = document.getElementById("b3");
    but.style.display = "";
}
//getting data
function getSelectData(str) {
    var select = document.getElementById(str);
    return +select.value.toString();
}
//Gets data from the tables
function LoadMatrix(r, c, str) {
    var matrix = [];
    for (let i = 0; i < r; i++) {
        matrix[i] = [];
        for (let j = 0; j < c; j++) {
            var nums = document.getElementById(str + i + j);
            var num = +nums.value;
            if (isNaN(num)) {
                alert("Enter valid data");
                num = 0;
            }
            matrix[i][j] = num;
        }
    }
    return matrix;
}
//add matrices
function add(matrix1, matrix2, r1, c1, r2, c2) {
    var proc = [];
    if (r1 == r2 && c1 == c2) { //checking if the 2 matrices can be add or not
        for (let i = 0; i < r1; i++) {
            proc[i] = [];
            for (let j = 0; j < c1; j++) {
                proc[i][j] = 0;
            }
        }
        for (let i = 0; i < r1; i++) {
            for (let j = 0; j < c1; j++) {
                proc[i][j] = matrix1[i][j] + matrix2[i][j];
            }
        }
        return proc;
    }
    else {
        alert("Sorry the matrices cannot be Added");
    }
}
//substract matrices
function sunstract(matrix1, matrix2, r1, c1, r2, c2) {
    var proc = [];
    if (r1 == r2 && c1 == c2) { //checking if the 2 matrices can be subs or not
        for (let i = 0; i < r1; i++) {
            proc[i] = [];
            for (let j = 0; j < c1; j++) {
                proc[i][j] = 0;
            }
        }
        for (let i = 0; i < r1; i++) {
            for (let j = 0; j < c1; j++) {
                proc[i][j] = matrix1[i][j] - matrix2[i][j];
            }
        }
        return proc;
    }
    else {
        alert("Sorry the matrices cannot be Substracted");
    }
}
//Multiply algorithm of the matrices
function mult(matrix1, matrix2, r1, c1, r2, c2) {
    var proc = [];
    if (c1 == r2) { //checking if the 2 matrices can be multiplied or not
        for (let i = 0; i < r1; i++) {
            proc[i] = [];
            for (let j = 0; j < c2; j++) {
                proc[i][j] = 0;
            }
        }
        for (let i = 0; i < r1; i++) {
            for (let j = 0; j < c2; j++) {
                for (let k = 0; k < r2; k++) {
                    proc[i][j] += matrix1[i][k] * matrix2[k][j];
                }
            }
        }
        return proc;
    }
    else {
        alert("Sorry the matrices cannot be multiplied");
    }
}
//Creates table for the Answer Matrix and prints the contents as well
function printResult() {
    row1 = getSelectData("Row1");
    col2 = getSelectData("Col2");
    console.log(row1);
    console.log(col2);
    var tab3 = document.getElementById("tb3");
    var labelTable = new LableTable(tab3, row1, col2, "c");
    labelTable.create();
    for (let i = 0; i < row1; i++) {
        for (let j = 0; j < col2; j++) {
            var label = document.getElementById("c" + i + j);
            label.innerHTML = mat3[i][j].toString();
        }
    }
}
//# sourceMappingURL=app.js.map