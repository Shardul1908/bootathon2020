//Getting data from page
var select : HTMLSelectElement = <HTMLSelectElement>document.getElementById("force");
addSelect(select);
var tab :HTMLTableElement = <HTMLTableElement>document.getElementById("tb1");
var forces1 : Forces = new Forces();

//function to populate select tag passed as parameter
function addSelect(sel : HTMLSelectElement){
    for(let i = 1;i<=10;i++) {
        let option :HTMLOptionElement = <HTMLOptionElement>document.createElement("option");
        option.text = i.toString();
        option.value = i.toString();
        sel.add(option);
    }
}

//Function for creating tables for the matrix.This is called by getForceTable()
function getTable(str: string,str1: string,id: string,row: number,col: number) {
    var p1 : HTMLParagraphElement = <HTMLParagraphElement>document.getElementById(str1); 
    p1.innerHTML = "Enter the Forces : <br>" 
    var inputTable : TextInputTable = new TextInputTable(tab,row,col,id);
    inputTable.create();
    inputTable.setText(id);
}

//onclick function for button Add matrix 2
function getForceTable() {
    var row : number = getSelectData("force");
    tab.style.display = "";
    var but : HTMLButtonElement = <HTMLButtonElement>document.getElementById("b1");
    but.style.display = "";
    getTable("tb1","p1","a",row,2);
}

//getting data
function getSelectData(str : string) {
    var select : HTMLSelectElement = <HTMLSelectElement>document.getElementById(str);
    return +select.value.toString();
}

//Gets data from the tables
function LoadForces(r:number,c:number,str : string) {
    for(let i =0;i<r;i++) {
        for(let j=0;j<c;j++) {
            var nums : HTMLInputElement = <HTMLInputElement>document.getElementById(str + i + j);
            var num : number = +nums.value;
            if(isNaN(num)) {
                alert("Enter valid data");
                num = 0;
            }
            if(j == 0) {
                var mag :number = num;
            }
            else{
                var dir : number = num;
            }
        }
        forces1.add(new Force(mag,dir));
    }
}

//calculates resultant
function getResultant() : void {
    var row : number = getSelectData("force");
    LoadForces(row,2,"a");
    var res : Force = forces1.calculateResultant();
    document.getElementById("ans").innerHTML = "The magnitude of the resultant = " + res.mag + "<br>The direction of the resultant is = " + res.dir;
}


