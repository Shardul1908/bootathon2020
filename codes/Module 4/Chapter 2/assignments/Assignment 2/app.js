//Getting data from page
var select = document.getElementById("force");
addSelect(select);
var tab = document.getElementById("tb1");
var forces1 = new Forces();
//function to populate select tag passed as parameter
function addSelect(sel) {
    for (let i = 1; i <= 10; i++) {
        let option = document.createElement("option");
        option.text = i.toString();
        option.value = i.toString();
        sel.add(option);
    }
}
//Function for creating tables for the matrix.This is called by getForceTable()
function getTable(str, str1, id, row, col) {
    var p1 = document.getElementById(str1);
    p1.innerHTML = "Enter the Forces : <br>";
    var inputTable = new TextInputTable(tab, row, col, id);
    inputTable.create();
    inputTable.setText(id);
}
//onclick function for button Add matrix 2
function getForceTable() {
    var row = getSelectData("force");
    tab.style.display = "";
    var but = document.getElementById("b1");
    but.style.display = "";
    getTable("tb1", "p1", "a", row, 2);
}
//getting data
function getSelectData(str) {
    var select = document.getElementById(str);
    return +select.value.toString();
}
//Gets data from the tables
function LoadForces(r, c, str) {
    for (let i = 0; i < r; i++) {
        for (let j = 0; j < c; j++) {
            var nums = document.getElementById(str + i + j);
            var num = +nums.value;
            if (isNaN(num)) {
                alert("Enter valid data");
                num = 0;
            }
            if (j == 0) {
                var mag = num;
            }
            else {
                var dir = num;
            }
        }
        forces1.add(new Force(mag, dir));
    }
}
//calculates resultant
function getResultant() {
    var row = getSelectData("force");
    LoadForces(row, 2, "a");
    var res = forces1.calculateResultant();
    document.getElementById("ans").innerHTML = "The magnitude of the resultant = " + res.mag + "<br>The direction of the resultant is = " + res.dir;
}
//# sourceMappingURL=app.js.map