//Class Force with mag and dir
class Force {
    private _magnitude : number;
    private _direction : number;

    constructor(magnitude : number , direction : number) {
        this._magnitude = magnitude;
        this._direction = direction;
    }

    //getters
    get mag() : number {
        return this._magnitude;
    }

    get dir() : number {
        return this._direction;
    }

    //setters
    set mag(magnitude : number) {
        this._magnitude= magnitude;
    }

    set dir(direction : number) {
        this._direction = direction;
    }

}

//Forces Array Class
class Forces {
    private forces : {force : Force}[];
    private index : number;

    constructor() {
        this.forces = [];
        this.index = 0;
    }

    //Add to the to the array
    add(force : Force){
        this.forces.push({force:force});
    }

    //Calculate the resultant of the forces in the array
    calculateResultant() : Force {
        var fx = this.sumFx();
        var fy = this.sumFy();
        var res : Force = new Force(0,0);
        res.mag = Math.sqrt(Math.pow(fx,2)+Math.pow(fy,2));
        res.dir = (180/Math.PI)*Math.atan2(fy,fx);
        return res;
    }

    //sumFx function
    private sumFx() {
        var sum : number = 0;
        for(let i = 0;i<this.forces.length;i++) {
            var fcos : number = this.forces[i].force.mag*Math.cos(this.forces[i].force.dir*(Math.PI/180));
            sum += fcos;
        }
        return sum;
    }

    //sumFy Function
    private sumFy() {
        var sum : number = 0;
        for(let i = 0;i<this.forces.length;i++) {
            var fsin : number = this.forces[i].force.mag*Math.sin(this.forces[i].force.dir*(Math.PI/180));
            sum += fsin;
        }
        return sum;
    }
}