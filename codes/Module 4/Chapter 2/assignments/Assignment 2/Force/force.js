//Class Force with mag and dir
class Force {
    constructor(magnitude, direction) {
        this._magnitude = magnitude;
        this._direction = direction;
    }
    //getters
    get mag() {
        return this._magnitude;
    }
    get dir() {
        return this._direction;
    }
    //setters
    set mag(magnitude) {
        this._magnitude = magnitude;
    }
    set dir(direction) {
        this._direction = direction;
    }
}
//Forces Array Class
class Forces {
    constructor() {
        this.forces = [];
        this.index = 0;
    }
    //Add to the to the array
    add(force) {
        this.forces.push({ force: force });
    }
    //Calculate the resultant of the forces in the array
    calculateResultant() {
        var fx = this.sumFx();
        var fy = this.sumFy();
        var res = new Force(0, 0);
        res.mag = Math.sqrt(Math.pow(fx, 2) + Math.pow(fy, 2));
        res.dir = (180 / Math.PI) * Math.atan2(fy, fx);
        return res;
    }
    //sumFx function
    sumFx() {
        var sum = 0;
        for (let i = 0; i < this.forces.length; i++) {
            var fcos = this.forces[i].force.mag * Math.cos(this.forces[i].force.dir * (Math.PI / 180));
            sum += fcos;
        }
        return sum;
    }
    //sumFy Function
    sumFy() {
        var sum = 0;
        for (let i = 0; i < this.forces.length; i++) {
            var fsin = this.forces[i].force.mag * Math.sin(this.forces[i].force.dir * (Math.PI / 180));
            sum += fsin;
        }
        return sum;
    }
}
//# sourceMappingURL=force.js.map