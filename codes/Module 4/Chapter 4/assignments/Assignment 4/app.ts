var tank : Geometry.Tank = new Geometry.Tank();
//setting canvas
var canvas : HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context : CanvasRenderingContext2D = canvas.getContext("2d");
//translate
context.translate(0,canvas.height);
context.scale(1,-1);
tank.draw(context);
var stop1 : boolean = false;

//fill clicked
function fill() {
    stop1 = false;
    anim();
}

//stop clicked
function stopit() {
    stop1 = true;
}

//animate
function anim() {
    context.clearRect(0,0,canvas.width,canvas.height);
    tank.fill();
    tank.draw(context);
    if(!stop1){
        window.requestAnimationFrame(anim);
    }
}