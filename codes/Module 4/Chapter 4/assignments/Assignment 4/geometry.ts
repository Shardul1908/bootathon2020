namespace Geometry {
    //class point
    export class Point {
        private _x : number;
        private _y : number;

        constructor(x:number,y:number) {
            this._x = x;
            this._y = y;
        }

        //getters
        get x() : number {
            return this._x;
        }

        get y() : number {
            return this._y;
        }

    }

    //class tank
    export class Tank {
        private container : Rectangle;
        private water : Rectangle;
        private full : boolean = false;
        private empty : boolean = true;

        constructor() {
            this.container = new Rectangle(new Point(300,200),100,300,true,false);
            this.water = new Rectangle(new Point(301,201),98,200,false,true);
        }

        draw(context : CanvasRenderingContext2D) {
            this.container.draw(context);
            this.water.draw(context);
        }

        //fill the tank with water
        fill() {
            console.log(this.water.h);
            if(this.empty){
                this.water.h++;
            }
            else if(this.full) {
                this.water.h--;
            }
            this.checker();
        }

        //check if full or empty
        private checker() {
            if(this.water.h > 299) {
                this.full = true;
                this.empty = false;
            }
            else if(this.water.h < 1) {
                this.full = false;
                this.empty = true;
            }
        }
    }

    //class rectangle
    class Rectangle {
        private srtPt : Point;
        private width : number;
        private height : number;
        private stroke : boolean;
        private fill : boolean;

        constructor(strPt : Point , width : number,height : number,stroke : boolean,fill : boolean) {
            this.srtPt = strPt;
            this.width = width;
            this.height = height;
            this.stroke = stroke;
            this.fill = fill;
        }

        //draw
        draw(context : CanvasRenderingContext2D) {
            context.beginPath();
            context.rect(this.srtPt.x,this.srtPt.y,this.width,this.height);
            //stroke if true
            if(this.stroke) {
                context.strokeStyle = "black";
                context.lineWidth = 2;
                context.stroke();
            }
            //fill if true
            if(this.fill) {
                context.fillStyle = "blue";
                context.fill();
            }
            context.closePath();
        }

        //setter and getter from height
        set h(h : number) {
            this.height = h;
        }

        get h() : number {
            return this.height;
        }
   }
}