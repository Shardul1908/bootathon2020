var Geometry;
(function (Geometry) {
    class Point {
        constructor(x, y) {
            this._x = x;
            this._y = y;
        }
        get x() {
            return this._x;
        }
        get y() {
            return this._y;
        }
    }
    Geometry.Point = Point;
    class Tank {
        constructor() {
            this.full = false;
            this.empty = true;
            this.container = new Rectangle(new Point(300, 200), 100, 300, true, false);
            this.water = new Rectangle(new Point(301, 201), 98, 200, false, true);
        }
        draw(context) {
            this.container.draw(context);
            this.water.draw(context);
        }
        fill() {
            console.log(this.water.h);
            if (this.empty) {
                this.water.h++;
            }
            else if (this.full) {
                this.water.h--;
            }
            this.checker();
        }
        checker() {
            if (this.water.h > 299) {
                this.full = true;
                this.empty = false;
            }
            else if (this.water.h < 1) {
                this.full = false;
                this.empty = true;
            }
        }
    }
    Geometry.Tank = Tank;
    class Rectangle {
        constructor(strPt, width, height, stroke, fill) {
            this.srtPt = strPt;
            this.width = width;
            this.height = height;
            this.stroke = stroke;
            this.fill = fill;
        }
        draw(context) {
            context.beginPath();
            context.rect(this.srtPt.x, this.srtPt.y, this.width, this.height);
            if (this.stroke) {
                context.strokeStyle = "black";
                context.lineWidth = 2;
                context.stroke();
            }
            if (this.fill) {
                context.fillStyle = "blue";
                context.fill();
            }
            context.closePath();
        }
        set h(h) {
            this.height = h;
        }
        get h() {
            return this.height;
        }
    }
})(Geometry || (Geometry = {}));
//# sourceMappingURL=geometry.js.map