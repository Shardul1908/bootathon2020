var tank = new Geometry.Tank();
var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
context.translate(0, canvas.height);
context.scale(1, -1);
tank.draw(context);
var stop1 = false;
function fill() {
    stop1 = false;
    anim();
}
function stopit() {
    stop1 = true;
}
function anim() {
    context.clearRect(0, 0, canvas.width, canvas.height);
    tank.fill();
    tank.draw(context);
    if (!stop1) {
        requestAnimationFrame(anim);
    }
}
//# sourceMappingURL=app.js.map