//setting canvas
var canvas : HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context : CanvasRenderingContext2D = canvas.getContext("2d");
//mouseclick event
canvas.addEventListener("click",mouseclick,false);
var shape : string;
var change : boolean = false;
//scene
var scene : Geometry.Scene = new Geometry.Scene(canvas);

let rect = canvas.getBoundingClientRect();
function mouseclick(e : MouseEvent) {

    let p1 : Geometry.Point = new Geometry.Point(e.clientX-rect.x,e.clientY-rect.y);

    //animate call
    if(change) {
        scene.find(p1);
        move();
    }
    //draw circles
    else{
        console.log(shape);
        if(shape == "circle") {
            var circle : Geometry.Circle = new Geometry.Circle(p1,1,25);
            circle.draw(context);
            scene.add(circle);
        }
        else
        if(shape == "ellipse") {
            var ell : Geometry.Ellipse = new Geometry.Ellipse(p1,1,50,20);
            ell.draw(context);
            scene.add(ell);
        }
    }
}

//circle clicked
function circle() {
    change = false;
    shape = "circle";
}

//ellipse clicked
function ellipse() {
    change = false;
    shape = "ellipse";
}

//move clicked
function move() {
    change = true;
    anim();
}

//animate
function anim() {
    context.clearRect(0,0,canvas.width,canvas.height);
    scene.move();
    scene.draw();
    window.requestAnimationFrame(anim);
}

