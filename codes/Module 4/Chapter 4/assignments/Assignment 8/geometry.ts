namespace Fire {
    //Class Point with x and y Coordinates
    export class Point {
        private _x : number;
        private _y : number;

        //constructor
        constructor(x:number,y:number) {
            this._x = x;
            this._y = y;
        }

        //Getters
        get x() : number {
            return this._x;
        }

        get y() : number {
            return this._y;
        }

        //setters
        set x(_x : number) {
            this._x = _x;
        }

        set y(_y : number) {
            this._y = _y;
        }
    }

    //class circle
    class Circle {
        private alpha : number;
        private radius : number;
        private x : number;
        private y : number;
        private velx : number;
        private vely : number;
        public color : string;
        constructor(radius : number,vx : number,vy : number,initx : number,inity : number) {
            this.radius = radius;
            this.x = initx;
            this.y = inity;
            this.velx = vx;
            this.vely = vy;
            this.alpha = 1;
            this.color = "red";
        }

        //draw circles
        draw(context : CanvasRenderingContext2D) {
            context.save();
            context.beginPath();
            context.arc(this.x,this.y,this.radius,0,2*Math.PI,true);
            context.fillStyle = this.color;
            context.globalAlpha = this.alpha;
            context.fill();
            context.restore();
            this.inc();
        }

        //incrementation
        private inc() {
            this.x = this.x + this.velx;
            this.y = this.y + this.vely;
            this.alpha = 0.98;
        }

        get Alpha() {
            return this.alpha;
        }
    }

    //class Smoke
    export class Smoke {
        private container : {circle : Circle}[] = [];
        private x : number;
        private y : number;
        private no : number;
        private context : CanvasRenderingContext2D;
        private canvas : HTMLCanvasElement;
        public vxrange : number;
        public vymin : number;
        public vymax : number;
        public radiusMin : number;
        public radiusMax : number;
        public color : string;

        constructor(canvas : HTMLCanvasElement,context : CanvasRenderingContext2D,x : number,y : number,no : number) {
            this.canvas = canvas;
            this.context = context;
            this.no = no;
            this.x = x;
            this.y = y;
            this.vxrange = 1;
            this.vymin = 1;
            this.vymax = 5;
            this.radiusMin = 3;
            this.radiusMax = 15;
            this.color = "white";
        }

        private random(min : number,max : number) {
            return (Math.random()*(max-min) + min);
        }

        //draw circles to make them look like fire animation
        startFire() {
            this.context.clearRect(0,0,this.canvas.width,this.canvas.height);
            this.context.beginPath();
            this.context.rect(0,0,this.canvas.width,this.canvas.height);
            this.context.fillStyle = "black";
            this.context.fill();
            this.context.save();
            this.context.translate(0,this.canvas.height);
            this.context.scale(1,-1);
            for(let i =0;i<=this.no;i++) {
                var vy = this.random(this.vymin,this.vymax);
                var vx = this.random(-this.vxrange,this.vxrange);
                var r = this.random(this.radiusMin,this.radiusMax);
                var circle : Circle = new Circle(r,vx,vy,this.x,this.y);
                circle.color = this.color;
                this.container.push({circle : circle});
            }

            for(let i =0;i<this.container.length;i++) {
                this.container[i].circle.draw(this.context);
            }

            this.context.restore();

            for(let i =0;i < this.container.length;i++) {
                if(this.container[i].circle.Alpha < 0.1) {
                    this.container.splice(i,1);
                    i--;
                }
            }
        }
    }
}