//setting the canvas
var canvas : HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context : CanvasRenderingContext2D = canvas.getContext("2d");

//smoke object
var smoke : Fire.Smoke = new Fire.Smoke(canvas,context,250,0,20);

smoke.color = "red";

runfire();

//animate
function runfire() {
    smoke.startFire();
    window.requestAnimationFrame(runfire);
}