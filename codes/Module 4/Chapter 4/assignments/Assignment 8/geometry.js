var Fire;
(function (Fire) {
    //Class Point with x and y Coordinates
    class Point {
        //constructor
        constructor(x, y) {
            this._x = x;
            this._y = y;
        }
        //Getters
        get x() {
            return this._x;
        }
        get y() {
            return this._y;
        }
        //setters
        set x(_x) {
            this._x = _x;
        }
        set y(_y) {
            this._y = _y;
        }
    }
    Fire.Point = Point;
    //class circle
    class Circle {
        constructor(radius, vx, vy, initx, inity) {
            this.radius = radius;
            this.x = initx;
            this.y = inity;
            this.velx = vx;
            this.vely = vy;
            this.alpha = 1;
            this.color = "red";
        }
        //draw circles
        draw(context) {
            context.save();
            context.beginPath();
            context.arc(this.x, this.y, this.radius, 0, 2 * Math.PI, true);
            context.fillStyle = this.color;
            context.globalAlpha = this.alpha;
            context.fill();
            context.restore();
            this.inc();
        }
        //incrementation
        inc() {
            this.x = this.x + this.velx;
            this.y = this.y + this.vely;
            this.alpha = 0.98;
        }
        get Alpha() {
            return this.alpha;
        }
    }
    //class Smoke
    class Smoke {
        constructor(canvas, context, x, y, no) {
            this.container = [];
            this.canvas = canvas;
            this.context = context;
            this.no = no;
            this.x = x;
            this.y = y;
            this.vxrange = 1;
            this.vymin = 1;
            this.vymax = 5;
            this.radiusMin = 3;
            this.radiusMax = 15;
            this.color = "white";
        }
        random(min, max) {
            return (Math.random() * (max - min) + min);
        }
        startFire() {
            this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.context.beginPath();
            this.context.rect(0, 0, this.canvas.width, this.canvas.height);
            this.context.fillStyle = "black";
            this.context.fill();
            this.context.save();
            this.context.translate(0, this.canvas.height);
            this.context.scale(1, -1);
            for (let i = 0; i <= this.no; i++) {
                var vy = this.random(this.vymin, this.vymax);
                var vx = this.random(-this.vxrange, this.vxrange);
                var r = this.random(this.radiusMin, this.radiusMax);
                var circle = new Circle(r, vx, vy, this.x, this.y);
                circle.color = this.color;
                this.container.push({ circle: circle });
            }
            for (let i = 0; i < this.container.length; i++) {
                this.container[i].circle.draw(this.context);
            }
            this.context.restore();
            for (let i = 0; i < this.container.length; i++) {
                if (this.container[i].circle.Alpha < 0.1) {
                    this.container.splice(i, 1);
                    i--;
                }
            }
        }
    }
    Fire.Smoke = Smoke;
})(Fire || (Fire = {}));
//# sourceMappingURL=geometry.js.map