namespace Geometry {
    //class point
    export class Point {
        private _x : number;
        private _y : number;

        constructor(x:number,y:number) {
            this._x = x;
            this._y = y;
        }

        //getters
        get x() : number {
            return this._x;
        }

        get y() : number {
            return this._y;
        }

        //setters
        set x(_x : number ) {
            this._x = _x;
        }

        set y(_y : number) {
            this._y = _y;
        }
    }

    //closePath class
    class ClosedPath {
        public points : Point[];
        protected color : string;
        protected whereTogo : string = "forward";
        protected point : Point;
        protected velocity : number;
        public move : boolean;

        constructor(arrayPoints:Point[],point : Point,velocity:number) {
            this.points = arrayPoints;
            this.color = "green";
            this.point = point;
            this.velocity = velocity;
            this.move = true;
        }

        //draw any generalised close path
        draw(context : CanvasRenderingContext2D) {
            context.beginPath();
            context.moveTo(this.points[0].x,this.points[0].y);
            for(let i = 0;i<this.points.length;i++) {
                context.lineTo(this.points[i].x,this.points[i].y);
            }
            context.lineTo(this.points[0].x,this.points[0].y);
            context.fillStyle = this.color;
            context.fill();
            context.strokeStyle = "Black";
            context.lineWidth = 2;
            context.stroke();
            context.closePath();   
        }

        //linear travel
        linearTravel() {
            if(this.whereTogo == "forward") {
                this.point.x = this.point.x + this.velocity;
            }
            else if(this.whereTogo == "backward"){
                this.point.x = this.point.x - this.velocity;
            } 
            this.checker();
        }

        //linear travel checker
        private checker() {
            if(this.point.x > 600) {
                this.whereTogo = "backward";
            }
            else if(this.point.x < 100) {
                this.whereTogo = "forward";
            }
        }
        
    }
    //generalised ellipse class 
    export class Ellipse extends ClosedPath{
        private majorAxis : number;
        private minorAxis : number;

        constructor(point : Point,velocity : number,majorAxis:number = 40,minorAxis : number = 20) {
            super([],point,velocity);
            this.majorAxis = majorAxis;
            this.minorAxis = minorAxis;
        }

        //calculate points for ellipse
        calculate() {
            this.points = [];
            for(let i= 0;i<=360;i++){
                let x1 = this.majorAxis * Math.cos(i*(Math.PI/180)) + this.point.x;
                let y1 = this.minorAxis * Math.sin(i*(Math.PI/180)) + this.point.y;
                this.points.push(new Point(x1,y1));
            }   
        }

        draw(context : CanvasRenderingContext2D) {
            this.calculate();
            super.draw(context);
        }
    }

    //class circle drawn by ellipse class but major-axis = minor-axis
    export class Circle extends Ellipse {
        constructor(center : Point,velocity : number,radius : number = 20) {
            super(center,velocity,radius,radius);
        }
    }

    //scene class for all particles
    export class Scene {
        public canvas : HTMLCanvasElement;
        public geometryContainer : {goemetry : ClosedPath}[];

        constructor(canvas : HTMLCanvasElement) {
            this.canvas = canvas;
            this.geometryContainer = [];
        }
        
        add(goemetry : ClosedPath) {
            this.geometryContainer.push({goemetry : goemetry});
            this.draw();
        }

        draw() {
            var context : CanvasRenderingContext2D = this.canvas.getContext("2d");
            context.clearRect(0,0,this.canvas.width,this.canvas.height);
            for(let i = 0;i<this.geometryContainer.length;i++) {
                this.geometryContainer[i].goemetry.draw(context);
            }
        }

        move() {
            for(let i = 0;i<this.geometryContainer.length;i++) {
                if(this.geometryContainer[i].goemetry.move == true) {
                    this.geometryContainer[i].goemetry.linearTravel();
                }
            }
        }
    }
}


