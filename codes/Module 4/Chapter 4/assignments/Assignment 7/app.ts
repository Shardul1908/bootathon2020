//setting canvas
var canvas : HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context : CanvasRenderingContext2D = canvas.getContext("2d");
var shape : string;
var change : boolean = false;
//scene
var scene : Geometry.Scene = new Geometry.Scene(canvas);
//draw particles
for(let i =0;i<150;i++) {
    var circle1 : Geometry.Circle = new Geometry.Circle(new Geometry.Point(random(100,500),random(100,200)),random(1,5),4);
    circle1.draw(context);
    scene.add(circle1);
}
anim();  

//animate
function anim() {
    context.clearRect(0,0,canvas.width,canvas.height);
    scene.move();
    scene.draw();
    window.requestAnimationFrame(anim);
}

//random function
function random(min : number , max : number) {
    return (Math.random() * (max - min) + min)
}