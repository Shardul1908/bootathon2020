var Geometry;
(function (Geometry) {
    class Point {
        constructor(x, y) {
            this._x = x;
            this._y = y;
        }
        get x() {
            return this._x;
        }
        get y() {
            return this._y;
        }
        set x(_x) {
            this._x = _x;
        }
        set y(_y) {
            this._y = _y;
        }
    }
    Geometry.Point = Point;
    class ClosedPath {
        constructor(arrayPoints, point, velocity) {
            this.whereTogo = "forward";
            this.points = arrayPoints;
            this.color = "green";
            this.point = point;
            this.velocity = velocity;
            this.move = true;
        }
        draw(context) {
            context.beginPath();
            context.moveTo(this.points[0].x, this.points[0].y);
            for (let i = 0; i < this.points.length; i++) {
                context.lineTo(this.points[i].x, this.points[i].y);
            }
            context.lineTo(this.points[0].x, this.points[0].y);
            context.fillStyle = this.color;
            context.fill();
            context.strokeStyle = "Black";
            context.lineWidth = 2;
            context.stroke();
            context.closePath();
        }
        linearTravel() {
            if (this.whereTogo == "forward") {
                this.point.x = this.point.x + this.velocity;
            }
            else if (this.whereTogo == "backward") {
                this.point.x = this.point.x - this.velocity;
            }
            this.checker();
        }
        checker() {
            if (this.point.x > 600) {
                this.whereTogo = "backward";
            }
            else if (this.point.x < 100) {
                this.whereTogo = "forward";
            }
        }
    }
    class Ellipse extends ClosedPath {
        constructor(point, velocity, majorAxis = 40, minorAxis = 20) {
            super([], point, velocity);
            this.majorAxis = majorAxis;
            this.minorAxis = minorAxis;
        }
        calculate() {
            this.points = [];
            for (let i = 0; i <= 360; i++) {
                let x1 = this.majorAxis * Math.cos(i * (Math.PI / 180)) + this.point.x;
                let y1 = this.minorAxis * Math.sin(i * (Math.PI / 180)) + this.point.y;
                this.points.push(new Point(x1, y1));
            }
        }
        draw(context) {
            this.calculate();
            super.draw(context);
        }
    }
    Geometry.Ellipse = Ellipse;
    class Circle extends Ellipse {
        constructor(center, velocity, radius = 20) {
            super(center, velocity, radius, radius);
        }
    }
    Geometry.Circle = Circle;
    class Scene {
        constructor(canvas) {
            this.canvas = canvas;
            this.geometryContainer = [];
        }
        add(goemetry) {
            this.geometryContainer.push({ goemetry: goemetry });
            this.draw();
        }
        draw() {
            var context = this.canvas.getContext("2d");
            context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            for (let i = 0; i < this.geometryContainer.length; i++) {
                this.geometryContainer[i].goemetry.draw(context);
            }
        }
        move() {
            for (let i = 0; i < this.geometryContainer.length; i++) {
                if (this.geometryContainer[i].goemetry.move == true) {
                    this.geometryContainer[i].goemetry.linearTravel();
                }
            }
        }
    }
    Geometry.Scene = Scene;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=geometry.js.map