var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
var shape;
var change = false;
var scene = new Geometry.Scene(canvas);
for (let i = 0; i < 150; i++) {
    var circle1 = new Geometry.Circle(new Geometry.Point(random(100, 500), random(100, 200)), random(1, 5), 4);
    circle1.draw(context);
    scene.add(circle1);
}
anim();
function anim() {
    context.clearRect(0, 0, canvas.width, canvas.height);
    scene.move();
    scene.draw();
    window.requestAnimationFrame(anim);
}
function random(min, max) {
    return (Math.random() * (max - min) + min);
}
//# sourceMappingURL=app.js.map