//namespace
var Geometry;
(function (Geometry) {
    //class Point
    class Point {
        constructor(x, y) {
            this._x = x;
            this._y = y;
        }
        //getters
        get x() {
            return this._x;
        }
        get y() {
            return this._y;
        }
        //setters
        set x(_x) {
            this._x = _x;
        }
        set y(_y) {
            this._y = _y;
        }
    }
    Geometry.Point = Point;
    //class circle
    class Circle {
        constructor(point, rad) {
            this.center = point;
            this.radius = rad;
        }
        //draw the circle
        draw(context) {
            context.beginPath();
            context.arc(this.center.x, this.center.y, 10, 0, 2 * Math.PI);
            context.strokeStyle = "black";
            context.stroke();
            context.fillStyle = "blue";
            context.fill();
            context.closePath();
        }
        set cent(pt) {
            this.center = pt;
        }
    }
    //sin Curve
    class SinCurve {
        constructor(pt, amp) {
            this.data = [];
            this.count = 0;
            this.xscale = 1.5;
            this.srtPt = pt;
            this.amp = amp;
            this.motion = "front";
            this.calculate();
        }
        //calculate all pts and store in array
        calculate() {
            for (let i = 0; i <= 360; i++) {
                var x = this.xscale * i;
                var y = this.amp * Math.sin(i * (Math.PI / 180));
                this.data.push(new Point(x, y));
                this.circ = new Circle(this.srtPt, 10);
            }
        }
        //draw curve and generate motion for the circle along the curve
        draw(context) {
            context.save();
            context.translate(this.srtPt.x, this.srtPt.y);
            context.scale(1, -1);
            context.beginPath();
            context.moveTo(this.data[0].x, this.data[0].y);
            for (let i = 1; i <= 360; i++) {
                context.lineTo(this.data[i].x, this.data[i].y);
            }
            context.strokeStyle = "red";
            context.stroke();
            this.circ.cent = new Point(this.data[this.count].x, this.data[this.count].y);
            this.circ.draw(context);
            this.check();
            //increments or decrements
            if (this.motion == "front") {
                this.count++;
            }
            else if (this.motion == "back") {
                this.count--;
            }
            console.log(this.motion);
            console.warn(this.count);
            context.restore();
        }
        //checker
        check() {
            if (this.count >= 360) {
                this.motion = "back";
            }
            else if (this.count <= 0) {
                this.motion = "front";
            }
        }
    }
    Geometry.SinCurve = SinCurve;
    class CosCurve {
        constructor(pt, amp) {
            this.data = [];
            this.count = 0;
            this.xscale = 1.5;
            this.srtPt = pt;
            this.amp = amp;
            this.motion = "front";
            this.calculate();
        }
        //calculate all pts and store in array
        calculate() {
            for (let i = 0; i <= 360; i++) {
                var x = this.xscale * i;
                var y = this.amp * Math.cos(i * (Math.PI / 180));
                this.data.push(new Point(x, y));
                this.circ = new Circle(this.srtPt, 10);
            }
        }
        //draw curve and generate motion for the circle along the curve
        draw(context) {
            context.save();
            context.translate(this.srtPt.x, this.srtPt.y);
            context.scale(1, -1);
            context.beginPath();
            context.moveTo(this.data[0].x, this.data[0].y);
            for (let i = 1; i <= 360; i++) {
                context.lineTo(this.data[i].x, this.data[i].y);
            }
            context.strokeStyle = "green";
            context.stroke();
            this.circ.cent = new Point(this.data[this.count].x, this.data[this.count].y);
            this.circ.draw(context);
            this.check();
            //increments or decrements
            if (this.motion == "front") {
                this.count++;
            }
            else if (this.motion == "back") {
                this.count--;
            }
            console.log(this.motion);
            console.warn(this.count);
            context.restore();
        }
        //checker
        check() {
            if (this.count >= 360) {
                this.motion = "back";
            }
            else if (this.count <= 0) {
                this.motion = "front";
            }
        }
    }
    Geometry.CosCurve = CosCurve;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=geometry.js.map