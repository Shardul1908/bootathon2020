//setting canvas
var canvas : HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context : CanvasRenderingContext2D = canvas.getContext("2d");
//sin curve
var sin : Geometry.SinCurve = new Geometry.SinCurve(new Geometry.Point(100,250),200);
//cos curve
var cos : Geometry.CosCurve = new Geometry.CosCurve(new Geometry.Point(100,250),200);
anim();

//animate
function anim() {
    context.clearRect(0,0,canvas.width,canvas.height);
    sin.draw(context);
    cos.draw(context);
    window.requestAnimationFrame(anim);
}
