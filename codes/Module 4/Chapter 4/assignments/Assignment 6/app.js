var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
var sin = new Geometry.SinCurve(new Geometry.Point(100, 250), 200);
var cos = new Geometry.CosCurve(new Geometry.Point(100, 250), 200);
anim();
function anim() {
    context.clearRect(0, 0, canvas.width, canvas.height);
    sin.draw(context);
    cos.draw(context);
    window.requestAnimationFrame(anim);
}
//# sourceMappingURL=app.js.map