var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
var wiper = new Geometry.Wiper();
wiper.draw(context);
canvas.addEventListener("click", mouseclick, false);
let rect = canvas.getBoundingClientRect();
function mouseclick(e) {
    let p1 = new Geometry.Point(e.clientX - rect.x, e.clientY - rect.y);
    wiper.start(p1);
    begin();
    context.clearRect(0, 0, canvas.width, canvas.height);
    wiper.draw(context);
}
function begin() {
    anim();
}
function anim() {
    wiper.move(context);
    requestAnimationFrame(anim);
}
//# sourceMappingURL=app.js.map