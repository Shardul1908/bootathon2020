namespace Geometry {
    //Class Point with x and y Coordinates
    export class Point {
        private _x : number;
        private _y : number;

        //constructor
        constructor(x:number,y:number) {
            this._x = x;
            this._y = y;
        }

        //Getters
        get x() : number {
            return this._x;
        }

        get y() : number {
            return this._y;
        }

        set x(_x : number) {
            this._x = _x;
        }

        set y(_y : number) {
            this._y = _y;
        }

    }

    //Class Circle
    class Circle {
        private center : Point;
        private radius : number;
        private strAngle : number;
        private endAngle : number;
        private _isOn : boolean;

        //constructor
        constructor(center : Point,radius : number,strAngle : number = 0,endAngle : number = 2*Math.PI) {
            this.center = center;
            this.radius = radius;
            this.strAngle = strAngle;
            this.endAngle = endAngle;
            this._isOn = false;
        }

        //draw the circle on a canvas
        draw(context : CanvasRenderingContext2D) {
            context.beginPath();
            context.arc(this.center.x,this.center.y,this.radius,this.strAngle,this.endAngle,true);
            context.strokeStyle = "black";
            if(this._isOn == false) {
                context.fillStyle = "gray";
            }
            else{
                context.fillStyle = "green";
            }
            context.fill();
            context.lineWidth = 2;
            context.stroke();
            context.closePath();
        }

        //is Inside function
        isInside(pt: Point) : boolean {
            var len : number = Math.sqrt(Math.pow((pt.x-this.center.x),2)+Math.pow((pt.y-this.center.y),2))
            if(len < this.radius) {
                return true;
            }
            return false;
        }

        //setter
        set isOn(_isOn : boolean) {
            this._isOn = _isOn;
        }

        get isOn() : boolean {
            return this._isOn;
        }
    }

    //Class Line
    class Line {
        private srtPt : Point;
        private endpt : Point;

        //constructor
        constructor(srtpt :Point,endPt : Point) {
            this.srtPt = srtpt;
            this.endpt = endPt;
        }

        //draw the line on the canvas
        draw(context : CanvasRenderingContext2D) {
            context.beginPath();
            context.moveTo(this.srtPt.x,this.srtPt.y);
            context.lineTo(this.endpt.x,this.endpt.y);
            context.strokeStyle = "black";
            context.lineWidth = 2;
            context.stroke();
            context.closePath();
        }
    }

    //Class Rectangle
    class Rectangle {
        private srtPt : Point;
        private width : number;
        private height : number;

        //constructor
        constructor(strPt : Point , width : number,height : number) {
            this.srtPt = strPt;
            this.width = width;
            this.height = height;
        }

        //draw the rectangle on the canvas
        draw(context : CanvasRenderingContext2D) {
            context.beginPath();
            context.rect(this.srtPt.x,this.srtPt.y,this.width,this.height);
            context.strokeStyle = "black";
            context.lineWidth = 2;
            context.stroke();
            context.fillStyle = "blue";
            context.fill();
            context.closePath();
        }
    }

    //class Wiper
    export class Wiper {
        private and : And;
        private frame : Rectangle;
        private wiper1 : Crank;
        private wiper2 : Crank;
        private isOn : boolean;

        constructor() {
            this.and = new And();
            this.frame = new Rectangle(new Point(460,250),15,100);
            this.wiper1 = new Crank(new Point(475,275),new Point(550,225),1);
            this.wiper2 = new Crank(new Point(475,325),new Point(550,275),1);
            this.isOn = false;
        }

        //draw all components of wiper
        draw(context : CanvasRenderingContext2D) {
            this.and.draw(context);
            this.frame.draw(context);
            this.wiper1.draw(context);
            this.wiper2.draw(context);
        }

        //start if on
        start(pt : Point) {
            this.and.check(pt);
            if(this.and.isOn) {
                this.isOn = true;
            }
            else{
                this.isOn = false;
            }
            console.log(this.isOn);
        }

        //generate motion
        move(context : CanvasRenderingContext2D) {
            if(this.isOn) {
                context.clearRect(0,0,canvas.width,canvas.height);
                this.wiper1.rotate();
                this.wiper2.rotate();
                this.draw(context);    
            }
        }

    }

    //Class AndGate
    class And {
        //its Elements
        private switch1 : Circle;
        private switch2 : Circle;
        private sem : Circle;
        private _isOn : boolean;

        //constructor
        constructor() {
            this.switch1 = new Circle(new Point(200,200),30);
            this.switch2 = new Circle(new Point(200,400),30);
            this.sem = new Circle(new Point(380,300),40,Math.PI/2,3*Math.PI/2);
            this._isOn = false;
        }

        //draw the wiper
        draw(context : CanvasRenderingContext2D) {
            this.switch1.draw(context);
            this.switch2.draw(context);
            var connection1 : Line = new Line(new Point(230,200),new Point(330,200));
            var connection2 : Line = new Line(new Point(230,400),new Point(330,400));
            connection1.draw(context);
            connection2.draw(context);
            connection1 = new Line(new Point(330,200),new Point(330,290));
            connection2 = new Line(new Point(330,400),new Point(330,310));
            connection1.draw(context);
            connection2.draw(context);
            connection1 = new Line(new Point(330,290),new Point(380,290));
            connection2 = new Line(new Point(330,310),new Point(380,310));
            connection1.draw(context);
            connection2.draw(context);
            this.sem.draw(context);
            var line : Line = new Line(new Point(380,260),new Point(380,340));
            line.draw(context);
            line = new Line(new Point(420,300),new Point(460,300));
            line.draw(context);
        }

        //check 
        //on when both inputs are true or 1
        check(pt : Point) {
            if(this.switch1.isInside(pt) && !this.switch1.isOn) {
                this.switch1.isOn = true;
            }
            else if(this.switch1.isInside(pt) && this.switch1.isOn) {
                this.switch1.isOn = false;
            }
            if(this.switch2.isInside(pt) && !this.switch2.isOn) {
                this.switch2.isOn = true;
            }
            else if(this.switch2.isInside(pt) && this.switch2.isOn) {
                this.switch2.isOn = false;
            }
            if(this.switch1.isOn && this.switch2.isOn) {
                this._isOn = true;
                this.sem.isOn = true;
            }
            else {
                this._isOn = false;
                this.sem.isOn = false;
            }
        }

        //getter
        get isOn() : boolean {
            return this._isOn;
        }
 
    }

    //crank for the wiper that will rotate in particular angle
    class Crank {
        private srtPt : Point;
        private endPt : Point;
        private angSpeed : number;
        private angle : number;
        private length : number;
        private movement : string;
        constructor(srtPt : Point,
                    endPt : Point,
                    angSpeed : number) {
                        this.srtPt = srtPt;
                        this.endPt = endPt;
                        this.angSpeed = angSpeed;
                        this.angle = this.getAng();
                        this.length = this.getLen();
                        this.movement = "up";
                    }

        draw(context : CanvasRenderingContext2D) {
            context.beginPath();
            context.moveTo(this.srtPt.x,this.srtPt.y);
            context.lineTo(this.endPt.x,this.endPt.y);
            context.lineWidth = 2;
            context.strokeStyle = "black";
            context.stroke();
            context.closePath();
        }

        private getAng() : number {
            return (180/Math.PI)*(Math.atan2((this.endPt.y-this.srtPt.y),(this.endPt.x-this.srtPt.x)));
        }

        private getLen() : number {
            return Math.sqrt(Math.pow((this.endPt.y-this.srtPt.y),2) + Math.pow((this.endPt.x-this.srtPt.x),2));
        }

        //Motion Generator
        rotate() {
            this.endPt.x = this.srtPt.x + (this.length*Math.cos(this.angle*(Math.PI/180)));
            this.endPt.y = this.srtPt.y + (this.length*Math.sin(this.angle*(Math.PI/180)));
            if(this.movement == "down"){
                this.angle = this.angle + this.angSpeed;
            }
            else if(this.movement == "up") {
                this.angle = this.angle - this.angSpeed;
            }
            console.log(this.angle);
            this.check();
        }

        //Motion Handler
        private check() {
            //console.log(this.angle)
            if(this.angle > 45) {
                this.movement = "up";
            }
            else if(this.angle < -45) {
                this.movement = "down";
            }
        }
    }
}