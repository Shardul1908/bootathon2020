//setting canvas
var canvas : HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context : CanvasRenderingContext2D = canvas.getContext("2d");

//wiper object
var wiper : Geometry.Wiper = new Geometry.Wiper();
wiper.draw(context);

//mouseclick event
canvas.addEventListener("click",mouseclick,false);

let rect = canvas.getBoundingClientRect();
function mouseclick(e : MouseEvent) {
    let p1 : Geometry.Point = new Geometry.Point(e.clientX-rect.x,e.clientY-rect.y);
    wiper.start(p1);
    begin();
    context.clearRect(0,0,canvas.width,canvas.height);
    wiper.draw(context);
}

//begin called when both circles clicked
function begin() {
    anim();
}

//animate
function anim() {
    wiper.move(context);
    window.requestAnimationFrame(anim);
}