var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
var imgid1 = document.getElementById("cat1");
var imgid2 = document.getElementById("duck1");
var btn1 = document.createElement("input");
btn1.value = "cat";
btn1.type = "button";
document.body.appendChild(btn1);
btn1.addEventListener("click", btn1click, false);
var btn2 = document.createElement("input");
btn2.value = "duct";
btn2.type = "button";
document.body.appendChild(btn2);
btn2.addEventListener("click", btn2click, false);
var animal = "none";
canvas.addEventListener("click", drawanimal, false);
function btn1click() {
    animal = "cat";
}
function btn2click() {
    animal = "duck";
}
function drawanimal(e) {
    if (animal == "cat") {
        let p1 = new Point(e.clientX, e.clientY);
        let i1 = new Img(imgid1, context, p1);
        i1.draw();
    }
    else if (animal == "duck") {
        let p1 = new Point(e.clientX, e.clientY);
        let i1 = new Img(imgid2, context, p1);
        i1.draw();
    }
}
//# sourceMappingURL=app.js.map