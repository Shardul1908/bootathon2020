class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}
class Img {
    constructor(imageid, context, p1) {
        this.loaded = false;
        this.context = context;
        this.img1 = imageid; //new Image(119, 79);
        this.p1 = p1;
        //this.img1.onload = function () {
        //    globalThis.draw(); 
        //}
        //this.img1.src = filepath;
        this.isanim = false;
    }
    draw() {
        this.context.drawImage(this.img1, this.p1.x, this.p1.y);
    }
}
//# sourceMappingURL=framework.js.map