var Geometry;
(function (Geometry) {
    //Class Point with x and y Coordinates
    class Point {
        //constructor
        constructor(x, y) {
            this._x = x;
            this._y = y;
        }
        //Getters
        get x() {
            return this._x;
        }
        get y() {
            return this._y;
        }
        //GETTERS
        set x(_x) {
            this._x = _x;
        }
        set y(_y) {
            this._y = _y;
        }
    }
    Geometry.Point = Point;
    //Class Ball
    class Ball {
        //constructor
        constructor(center, e = 0.8) {
            this.center = center;
            this.radius = 15;
            this.xVelocity = 1;
            this.yVelocity = 0;
            this.g = 0.1;
            this.speed = 0;
            this.e = e;
            this.h = 484; //exact ground for the ball with radius 15
        }
        //draw the circle on a canvas
        draw(context) {
            context.beginPath();
            context.arc(this.center.x, this.center.y, this.radius, 0, 2 * Math.PI);
            context.strokeStyle = "black";
            context.fillStyle = "green";
            context.fill();
            context.lineWidth = 2;
            context.stroke();
            context.closePath();
        }
        bounce() {
            this.center.x += this.xVelocity;
            this.speed += this.g;
            this.center.y += (this.yVelocity + this.speed);
            if (this.center.y > this.h) {
                this.center.y = this.h;
                this.speed = -(this.speed * this.e);
            }
        }
    }
    Geometry.Ball = Ball;
    class Scene {
        constructor(canvas) {
            this.canvas = canvas;
            this.geometryContainer = [];
        }
        add(goemetry) {
            this.geometryContainer.push({ goemetry: goemetry });
            this.draw();
        }
        draw() {
            var context = this.canvas.getContext("2d");
            context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            for (let i = 0; i < this.geometryContainer.length; i++) {
                this.geometryContainer[i].goemetry.draw(context);
                this.geometryContainer[i].goemetry.bounce();
            }
        }
    }
    Geometry.Scene = Scene;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=geometry.js.map