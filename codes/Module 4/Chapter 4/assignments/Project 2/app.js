var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
var scene = new Geometry.Scene(canvas);
var ball = new Geometry.Ball(new Geometry.Point(150, 250));
ball.draw(context);
anim();
scene.add(ball);
let rect = canvas.getBoundingClientRect();
canvas.addEventListener("click", mouseclick, false);
function mouseclick(e) {
    let p1 = new Geometry.Point(e.clientX - rect.x, e.clientY - rect.y);
    ball = new Geometry.Ball(p1);
    ball.draw(context);
    scene.add(ball);
}
function begin() {
    anim();
}
function anim() {
    context.clearRect(0, 0, canvas.width, canvas.height);
    scene.draw();
    window.requestAnimationFrame(anim);
}
//# sourceMappingURL=app.js.map