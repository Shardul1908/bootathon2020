//setting canvas
var canvas : HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context : CanvasRenderingContext2D = canvas.getContext("2d");
var scene : Geometry.Scene = new Geometry.Scene(canvas);
//creating a ball object
var ball : Geometry.Ball = new Geometry.Ball(new Geometry.Point(150,250));
ball.draw(context);
anim();
scene.add(ball);

//mouseclick handler
let rect = canvas.getBoundingClientRect();
canvas.addEventListener("click",mouseclick,false);
function mouseclick(e : MouseEvent) {
    //throws ball from the height clicked by user
    let p1 : Geometry.Point = new Geometry.Point(e.clientX-rect.x,e.clientY-rect.y);
    ball = new Geometry.Ball(p1);
    ball.draw(context);
    scene.add(ball);
}

//begin 
function begin() {
    anim();
}

//animate
function anim() {
    context.clearRect(0,0,canvas.width,canvas.height);
    scene.draw();
    window.requestAnimationFrame(anim);
}