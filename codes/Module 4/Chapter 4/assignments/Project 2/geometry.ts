namespace Geometry {
    //Class Point with x and y Coordinates
    export class Point {
        private _x : number;
        private _y : number;

        //constructor
        constructor(x:number,y:number) {
            this._x = x;
            this._y = y;
        }

        //Getters
        get x() : number {
            return this._x;
        }

        get y() : number {
            return this._y;
        }

        //GETTERS
        set x(_x : number) {
            this._x = _x;
        }

        set y(_y : number) {
            this._y = _y;
        }

    }

    //Class Ball
    export class Ball {
        private center : Point;
        private radius : number;
        private xVelocity : number;
        private yVelocity : number;
        private g : number;
        private speed : number;
        private e : number;
        private h : number;

        //constructor
        constructor(center : Point,e : number = 0.8) {
            this.center = center;
            this.radius = 15;
            this.xVelocity = 1;
            this.yVelocity = 0;
            this.g = 0.1;
            this.speed = 0;
            this.e = e;
            this.h = 484;           //exact ground for the ball with radius 15
        }

        //draw the circle on a canvas
        draw(context : CanvasRenderingContext2D) {
            context.beginPath();
            context.arc(this.center.x,this.center.y,this.radius,0,2*Math.PI);
            context.strokeStyle = "black";
            context.fillStyle = "green";
            context.fill();
            context.lineWidth = 2;
            context.stroke();
            context.closePath();
        }

        //bounce the ball when touches the ground
        bounce() {
            this.center.x += this.xVelocity;
            this.speed += this.g;
            this.center.y += (this.yVelocity + this.speed);
            if(this.center.y > this.h) {
                this.center.y = this.h;
                this.speed = -(this.speed*this.e);
            }
        }

    }

    //scene for drawing multiple balls
    export class Scene {
        public canvas : HTMLCanvasElement;
        public geometryContainer : {goemetry : Ball}[];

        constructor(canvas : HTMLCanvasElement) {
            this.canvas = canvas;
            this.geometryContainer = [];
        }
        
        add(goemetry : Ball) {
            this.geometryContainer.push({goemetry : goemetry});
            this.draw();
        }

        draw() {
            var context : CanvasRenderingContext2D = this.canvas.getContext("2d");
            context.clearRect(0,0,this.canvas.width,this.canvas.height);
            for(let i = 0;i<this.geometryContainer.length;i++) {
                this.geometryContainer[i].goemetry.draw(context);
                this.geometryContainer[i].goemetry.bounce();
            }
        }
    }
}