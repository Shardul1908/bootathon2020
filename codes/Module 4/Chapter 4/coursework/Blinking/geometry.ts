namespace Geometry {
    export class Point {
        private _x : number;
        private _y : number;

        constructor(x:number,y:number) {
            this._x = x;
            this._y = y;
        }

        get x() : number {
            return this._x;
        }

        get y() : number {
            return this._y;
        }

        set x(_x : number ) {
            this._x = _x;
        }

        set y(_y : number) {
            this._y = _y;
        }
    }

    class ClosedPath {
        public points : Point[];
        protected color : string;
        protected whereTogo : string = "forward";
        protected point : Point;
        protected velocity : number;
        protected move : boolean;

        set mov(move : boolean) {
            this.move = move;
        }

        get mov() : boolean {
            return this.move;
        }

        constructor(arrayPoints:Point[],point : Point,velocity:number) {
            this.points = arrayPoints;
            this.color = "green";
            this.point = point;
            this.velocity = velocity;
        }

        draw(context : CanvasRenderingContext2D) {
            context.beginPath();
            context.moveTo(this.points[0].x,this.points[0].y);
            for(let i = 0;i<this.points.length;i++) {
                context.lineTo(this.points[i].x,this.points[i].y);
            }
            context.lineTo(this.points[0].x,this.points[0].y);
            context.fillStyle = this.color;
            context.fill();
            context.strokeStyle = "Black";
            context.lineWidth = 2;
            context.stroke();
            context.closePath();   
        }

        linearTravel() {
            if(move) {
                if(this.whereTogo == "forward") {
                    this.point.x = this.point.x + this.velocity;
                }
                else if(this.whereTogo == "backward"){
                    this.point.x = this.point.x - this.velocity;
                } 
                this.checker();
            }
        }

        private checker() {
            if(this.color == "green") {
                this.color = "yellow";
            }
            else {
                this.color = "green";
            }
            if(this.point.x > 600) {
                this.whereTogo = "backward";
            }
            else if(this.point.x < 100) {
                this.whereTogo = "forward";
            }
        }
        
    }

    export class Ellipse extends ClosedPath{
        private majorAxis : number;
        private minorAxis : number;

        constructor(point : Point,velocity : number,majorAxis:number = 40,minorAxis : number = 20) {
            super([],point,velocity);
            this.majorAxis = majorAxis;
            this.minorAxis = minorAxis;
        }

        calculate() {
            this.points = [];
            for(let i= 0;i<=360;i++){
                let x1 = this.majorAxis * Math.cos(i*(Math.PI/180)) + this.point.x;
                let y1 = this.minorAxis * Math.sin(i*(Math.PI/180)) + this.point.y;
                this.points.push(new Point(x1,y1));
            }   
        }

        draw(context : CanvasRenderingContext2D) {
            this.calculate();
            super.draw(context);
        }
    }

    export class Circle extends Ellipse {
        constructor(center : Point,velocity : number,radius : number = 20) {
            super(center,velocity,radius,radius);
        }
    }

    class ClosedPathProperties {
        public centroidX : number;
        public centroidY : number;
        public area : number;
        public points : Point[];

        constructor(shape : ClosedPath) {
            this.points = shape.points;
        }

        private centroid() {
            var area = 0;
            var cx = 0;
            var cy = 0;
            var a = 0;

            for(let i = 0;i<this.points.length-1;i++) {
                a = (this.points[i].x * this.points[i+1].y) - 
                    (this.points[i+1].x * this.points[i].y);

                cx = cx  + (this.points[i].x + this.points[i+1].x)*a;
                cy = cy  + (this.points[i].y + this.points[i+1].y)*a;
                area = area  + a;
            }
            this.area = area/2;
            this.centroidX = cx/(6*area/2);
            this.centroidY = cy/(6*area/2);
        }

        public getCentroid() : Point{
            this.centroid();
            return new Point(this.centroidX,this.centroidY);
        }

        public getArea() {
            this.centroid();
            return this.area;
        }

        private getTraingleArea(userPoint : Point,point1 : Point,point2 : Point) {
            var area = (userPoint.x * (point1.y-point2.y) + point1.x * (point2.y-userPoint.y) + point2.x * (userPoint.y-point1.y))/2;
            return Math.abs(area);
        } 

        public isInside(point : Point) : boolean {
            var triArea = 0;
            for(let i =0;i<this.points.length-1;i++) {
                triArea += this.getTraingleArea(point,this.points[i],this.points[i+1]);
            }
            this.centroid();
            if(Math.abs(triArea-this.area) < 0.000001) {
                return true;
            }
            return false;
        }
    }

    export class Scene {
        public canvas : HTMLCanvasElement;
        public geometryContainer : {goemetry : ClosedPath}[];

        constructor(canvas : HTMLCanvasElement) {
            this.canvas = canvas;
            this.geometryContainer = [];
        }
        
        add(goemetry : ClosedPath) {
            this.geometryContainer.push({goemetry : goemetry});
            this.draw();
        }

        draw() {
            var context : CanvasRenderingContext2D = this.canvas.getContext("2d");
            context.clearRect(0,0,this.canvas.width,this.canvas.height);
            for(let i = 0;i<this.geometryContainer.length;i++) {
                this.geometryContainer[i].goemetry.draw(context);
            }
        }

        find(point : Point) {
            for(let i = 0;i<this.geometryContainer.length;i++) {
                var prop : ClosedPathProperties = new ClosedPathProperties(this.geometryContainer[i].goemetry);
                if(prop.isInside(point)) {
                    this.geometryContainer[i].goemetry.mov = true;
                }
            }
        }

        move() {
            for(let i = 0;i<this.geometryContainer.length;i++) {
                if(this.geometryContainer[i].goemetry.mov == true) {
                    this.geometryContainer[i].goemetry.linearTravel();
                }
            }
        }
    }
}


