var Geometry;
(function (Geometry) {
    class Point {
        constructor(x, y) {
            this._x = x;
            this._y = y;
        }
        get x() {
            return this._x;
        }
        get y() {
            return this._y;
        }
        set x(_x) {
            this._x = _x;
        }
        set y(_y) {
            this._y = _y;
        }
    }
    Geometry.Point = Point;
    class ClosedPath {
        constructor(arrayPoints, point, velocity) {
            this.whereTogo = "forward";
            this.points = arrayPoints;
            this.color = "green";
            this.point = point;
            this.velocity = velocity;
        }
        set mov(move) {
            this.move = move;
        }
        get mov() {
            return this.move;
        }
        draw(context) {
            context.beginPath();
            context.moveTo(this.points[0].x, this.points[0].y);
            for (let i = 0; i < this.points.length; i++) {
                context.lineTo(this.points[i].x, this.points[i].y);
            }
            context.lineTo(this.points[0].x, this.points[0].y);
            context.fillStyle = this.color;
            context.fill();
            context.strokeStyle = "Black";
            context.lineWidth = 2;
            context.stroke();
            context.closePath();
        }
        linearTravel() {
            if (move) {
                if (this.whereTogo == "forward") {
                    this.point.x = this.point.x + this.velocity;
                }
                else if (this.whereTogo == "backward") {
                    this.point.x = this.point.x - this.velocity;
                }
                this.checker();
            }
        }
        checker() {
            if (this.color == "green") {
                this.color = "yellow";
            }
            else {
                this.color = "green";
            }
            if (this.point.x > 600) {
                this.whereTogo = "backward";
            }
            else if (this.point.x < 100) {
                this.whereTogo = "forward";
            }
        }
    }
    class Ellipse extends ClosedPath {
        constructor(point, velocity, majorAxis = 40, minorAxis = 20) {
            super([], point, velocity);
            this.majorAxis = majorAxis;
            this.minorAxis = minorAxis;
        }
        calculate() {
            this.points = [];
            for (let i = 0; i <= 360; i++) {
                let x1 = this.majorAxis * Math.cos(i * (Math.PI / 180)) + this.point.x;
                let y1 = this.minorAxis * Math.sin(i * (Math.PI / 180)) + this.point.y;
                this.points.push(new Point(x1, y1));
            }
        }
        draw(context) {
            this.calculate();
            super.draw(context);
        }
    }
    Geometry.Ellipse = Ellipse;
    class Circle extends Ellipse {
        constructor(center, velocity, radius = 20) {
            super(center, velocity, radius, radius);
        }
    }
    Geometry.Circle = Circle;
    class ClosedPathProperties {
        constructor(shape) {
            this.points = shape.points;
        }
        centroid() {
            var area = 0;
            var cx = 0;
            var cy = 0;
            var a = 0;
            for (let i = 0; i < this.points.length - 1; i++) {
                a = (this.points[i].x * this.points[i + 1].y) -
                    (this.points[i + 1].x * this.points[i].y);
                cx = cx + (this.points[i].x + this.points[i + 1].x) * a;
                cy = cy + (this.points[i].y + this.points[i + 1].y) * a;
                area = area + a;
            }
            this.area = area / 2;
            this.centroidX = cx / (6 * area / 2);
            this.centroidY = cy / (6 * area / 2);
        }
        getCentroid() {
            this.centroid();
            return new Point(this.centroidX, this.centroidY);
        }
        getArea() {
            this.centroid();
            return this.area;
        }
        getTraingleArea(userPoint, point1, point2) {
            var area = (userPoint.x * (point1.y - point2.y) + point1.x * (point2.y - userPoint.y) + point2.x * (userPoint.y - point1.y)) / 2;
            return Math.abs(area);
        }
        isInside(point) {
            var triArea = 0;
            for (let i = 0; i < this.points.length - 1; i++) {
                triArea += this.getTraingleArea(point, this.points[i], this.points[i + 1]);
            }
            this.centroid();
            if (Math.abs(triArea - this.area) < 0.000001) {
                return true;
            }
            return false;
        }
    }
    class Scene {
        constructor(canvas) {
            this.canvas = canvas;
            this.geometryContainer = [];
        }
        add(goemetry) {
            this.geometryContainer.push({ goemetry: goemetry });
            this.draw();
        }
        draw() {
            var context = this.canvas.getContext("2d");
            context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            for (let i = 0; i < this.geometryContainer.length; i++) {
                this.geometryContainer[i].goemetry.draw(context);
            }
        }
        find(point) {
            for (let i = 0; i < this.geometryContainer.length; i++) {
                var prop = new ClosedPathProperties(this.geometryContainer[i].goemetry);
                if (prop.isInside(point)) {
                    this.geometryContainer[i].goemetry.mov = true;
                }
            }
        }
        move() {
            for (let i = 0; i < this.geometryContainer.length; i++) {
                if (this.geometryContainer[i].goemetry.mov == true) {
                    this.geometryContainer[i].goemetry.linearTravel();
                }
            }
        }
    }
    Geometry.Scene = Scene;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=geometry.js.map