var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
canvas.addEventListener("click", mouseclick, false);
var shape;
var input = document.getElementById("t1");
var input3 = document.getElementById("t3");
var input4 = document.getElementById("t4");
var input5 = document.getElementById("t5");
var change = false;
var scene = new Geometry.Scene(canvas);
let rect = canvas.getBoundingClientRect();
function mouseclick(e) {
    let p1 = new Geometry.Point(e.clientX - rect.x, e.clientY - rect.y);
    if (change) {
        scene.find(p1);
        move();
    }
    else {
        console.log(shape);
        if (shape == "circle") {
            var circle = new Geometry.Circle(p1, 1, 25);
            circle.draw(context);
            scene.add(circle);
        }
        else if (shape == "ellipse") {
            var ell = new Geometry.Ellipse(p1, 1, 50, 20);
            ell.draw(context);
            scene.add(ell);
        }
    }
}
function circle() {
    change = false;
    shape = "circle";
}
function ellipse() {
    change = false;
    shape = "ellipse";
}
function move() {
    change = true;
    anim();
}
function anim() {
    context.clearRect(0, 0, canvas.width, canvas.height);
    scene.move();
    scene.draw();
    requestAnimationFrame(anim);
}
//# sourceMappingURL=app.js.map