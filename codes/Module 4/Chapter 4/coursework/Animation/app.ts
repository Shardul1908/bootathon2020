var canvas : HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context : CanvasRenderingContext2D = canvas.getContext("2d");
context.translate(0,canvas.height);
context.scale(1,-1);

var crank : Geometry.Crank = new Geometry.Crank(new Geometry.Point(300,300),new Geometry.Point(400,400),1,1);
crank.draw(context);

function rotate() {
    anim();
}

function anim() {
    context.clearRect(0,0,canvas.width,canvas.height);
    crank.rotate();
    crank.linearTravel();
    crank.draw(context);
    window.requestAnimationFrame(anim);
}