var Geometry;
(function (Geometry) {
    class Point {
        constructor(_x, _y) {
            this._x = _x;
            this._y = _y;
        }
        get x() {
            return this._x;
        }
        get y() {
            return this._y;
        }
        set x(_x) {
            this._x = _x;
        }
        set y(_y) {
            this._y = _y;
        }
    }
    Geometry.Point = Point;
    class Crank {
        constructor(srtPt, endPt, angSpeed, velocity) {
            this.whereTogo = "forward";
            this.srtPt = srtPt;
            this.endPt = endPt;
            this.angSpeed = angSpeed;
            this.velocity = velocity;
            this.angle = this.getAng();
            this.length = this.getLen();
        }
        draw(context) {
            context.beginPath();
            context.moveTo(this.srtPt.x, this.srtPt.y);
            context.lineTo(this.endPt.x, this.endPt.y);
            context.lineWidth = 2;
            context.strokeStyle = "black";
            context.stroke();
            context.closePath();
        }
        getAng() {
            return (180 / Math.PI) * (Math.atan2((this.endPt.y - this.srtPt.y), (this.endPt.x - this.srtPt.x)));
        }
        getLen() {
            return Math.sqrt(Math.pow((this.endPt.y - this.srtPt.y), 2) + Math.pow((this.endPt.x - this.srtPt.x), 2));
        }
        //Motion Generator
        rotate() {
            this.endPt.x = this.srtPt.x + (this.length * Math.cos(this.angle * (Math.PI / 180)));
            this.endPt.y = this.srtPt.y + (this.length * Math.sin(this.angle * (Math.PI / 180)));
            this.angle = this.angle + this.angSpeed;
            this.check();
        }
        //Motion Handler
        check() {
            //console.log(this.angle)
            if (this.angle >= 360) {
                this.angle = 0;
            }
        }
        linearTravel() {
            if (this.whereTogo == "forward") {
                this.srtPt.x = this.srtPt.x + this.velocity;
            }
            else if (this.whereTogo == "backward") {
                this.srtPt.x = this.srtPt.x - this.velocity;
            }
            this.checker();
        }
        checker() {
            console.log(this.srtPt.x);
            if (this.srtPt.x > 600) {
                this.whereTogo = "backward";
            }
            else if (this.srtPt.x < 100) {
                this.whereTogo = "forward";
            }
        }
    }
    Geometry.Crank = Crank;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=geometry.js.map