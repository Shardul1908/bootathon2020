/*  Master Class Table
    Has tableElement , rows , Columns , Cells, Id
    Has function to create the Table with rows with generic cellDataType , id
    and to clear table
*/
class Table {
    constructor(table, rows, columns, cellDataType, id) {
        this.table = table;
        this.rows = rows;
        this.columns = columns;
        this.cellDataType = cellDataType;
        this.id = id;
    }
    createTable() {
        for (let i = 0; i < this.rows; i++) {
            var row = this.table.insertRow();
            for (let j = 0; j < this.columns; j++) {
                var cell = row.insertCell();
                var type = document.createElement(this.cellDataType);
                type.id = this.id + i.toString() + j.toString();
                cell.appendChild(type);
            }
        }
    }
    clearTable() {
        while (this.table.rows.length > 1) {
            this.table.deleteRow(1);
        }
    }
}
class LableTable extends Table {
    constructor(table, rows, columns, id) {
        super(table, rows, columns, "label", id);
    }
    create() {
        this.clearTable();
        this.createTable();
    }
    setText(id) {
        for (let i = 0; i < this.rows; i++) {
            for (let j = 0; j < this.columns; j++) {
                var label = document.getElementById(id + i.toString() + j.toString());
                label.innerHTML = i + " " + j;
            }
        }
    }
}
class InputTable extends Table {
    constructor(table, rows, columns, id, type) {
        super(table, rows, columns, "input", id);
        this.type = type;
        this._id = id;
    }
    setType() {
        for (let i = 0; i < this.rows; i++) {
            for (let j = 0; j < this.columns; j++) {
                var element = document.getElementById(this._id + i.toString() + j.toString());
                element.type = this.type;
            }
        }
    }
    create() {
        this.clearTable();
        this.createTable();
    }
}
class TextInputTable extends InputTable {
    constructor(table, rows, columns, id) {
        super(table, rows, columns, id, "text");
    }
    create() {
        super.create();
        super.setType();
    }
    setText(id) {
        for (let i = 0; i < this.rows; i++) {
            for (let j = 0; j < this.columns; j++) {
                var input = document.getElementById(id + i.toString() + j.toString());
                if (j == 0) {
                    input.placeholder = "Force " + (i + 1).toString() + " : Magnitude";
                }
                else if (j == 1) {
                    input.placeholder = "Force " + (i + 1).toString() + " : Direction";
                }
            }
        }
    }
}
//# sourceMappingURL=TableClass.js.map