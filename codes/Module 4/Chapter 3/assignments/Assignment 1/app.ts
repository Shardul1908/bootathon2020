var canvas : HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context : CanvasRenderingContext2D = canvas.getContext("2d");
var forces : {magnitude:number,direction:number}[] = [];
var select : HTMLSelectElement = <HTMLSelectElement>document.getElementById("force");
addSelect(select);
var tab :HTMLTableElement = <HTMLTableElement>document.getElementById("tb1");

//function to populate select tag passed as parameter
function addSelect(sel : HTMLSelectElement){
    for(let i = 1;i<=10;i++) {
        let option :HTMLOptionElement = <HTMLOptionElement>document.createElement("option");
        option.text = i.toString();
        option.value = i.toString();
        sel.add(option);
    }
}

//Function for creating tables for the matrix.This is called by getForceTable()
function getTable(str: string,str1: string,id: string,row: number,col: number) {
    console.log(row);
    console.log(col);
    var p1 : HTMLParagraphElement = <HTMLParagraphElement>document.getElementById(str1); 
    p1.innerHTML = "Enter the Forces : <br>" 
    var inputTable : TextInputTable = new TextInputTable(tab,row,col,id);
    inputTable.create();
    inputTable.setText(id);
}

//onclick function for button Add matrix 2
function getForceTable() {
    var row : number = getSelectData("force");
    console.log(row);
    tab.style.display = "";
    var but : HTMLButtonElement = <HTMLButtonElement>document.getElementById("b1");
    but.style.display = "";
    getTable("tb1","p1","a",row,2);
}

//getting data
function getSelectData(str : string) {
    var select : HTMLSelectElement = <HTMLSelectElement>document.getElementById(str);
    return +select.value.toString();
}

//Gets data from the tables
function LoadForces(r:number,c:number,str : string) : {magnitude:number,direction:number}[] {
    var temp : {magnitude:number,direction:number}[] = [];
    for(let i =0;i<r;i++) {
        for(let j=0;j<c;j++) {
            var nums : HTMLInputElement = <HTMLInputElement>document.getElementById(str + i + j);
            var num : number = +nums.value;
            if(isNaN(num)) {
                alert("Enter valid data");
                num = 0;
            }
            if(j == 0) {
                var mag = num;
            }
            else{
                var ang = num;
            }
        }
        temp.push({magnitude : mag,direction : ang});
    }
    return temp;
}


//Gets the sum of the forces in x or y directions
function sum1(getType:number) : number {
    var sums:number = 0;
    if(getType == 1) {                         //calculate summation fx
        for(let i = 0;i<forces.length;i++) {
            var fcos : number = forces[i].magnitude*Math.cos(forces[i].direction*(Math.PI/180));
            sums += fcos;
        }
    }
    else if(getType == 2) {                     //calculate summation fy
        for(let i = 0;i<forces.length;i++) {
            var fsin : number = forces[i].magnitude*Math.sin(forces[i].direction*(Math.PI/180));
            sums += fsin;
        }
    }
    return sums;
}

//calculates resultant
function getResultant() : void {
    canvas.style.display= "";
    drawForces();
    var row : number = getSelectData("force");
    forces = LoadForces(row,2,"a");
    var fx : number = sum1(1);              //1 for summation fx
    var fy : number = sum1(2);              //2 for summation fy
    var resmag : number = Math.sqrt(Math.pow(fx,2)+Math.pow(fy,2));
    var resDir : number = (180/Math.PI)*Math.atan2(fy,fx);
    document.getElementById("ans").innerHTML = "The magnitude of the resultant = " + resmag + "<br>The direction of the resultant is = " + resDir;
}

function drawForces() { 
    context.clearRect(0,0,canvas.width,canvas.height);
    var row : number = getSelectData("force");
    forces = LoadForces(row,2,"a");
    var max : number = forces[0].magnitude;
    var min : number = forces[0].magnitude;
    for(let i = 1;i<forces.length;i++) {
        if(max < forces[i].magnitude){
            max = forces[i].magnitude;
        }
        if(min > forces[i].magnitude) {
            min = forces[i].magnitude;
        }
    }
    console.warn(max + " " + min);
    var scale = (min/max);
    if(max < 20) {
        scale = scale*100
    }
    console.warn(scale);
    context.beginPath();
    context.moveTo(200,300);
    var x : number = 200;
    var y : number = 300;
    var nx : number;
    var  ny : number;
    for(let i = 0;i<forces.length;i++) {
        console.log(i);
        nx = forces[i].magnitude*scale*Math.cos(-forces[i].direction*(Math.PI/180));
        ny = forces[i].magnitude*scale*Math.sin(-forces[i].direction*(Math.PI/180));
        context.lineTo(200+nx,300+ny);
        x = nx;
        y = ny;
    }
    context.lineTo(200,300);
    context.strokeStyle = "black";
    context.lineWidth = 2;
    context.stroke();
    context.closePath();

}
