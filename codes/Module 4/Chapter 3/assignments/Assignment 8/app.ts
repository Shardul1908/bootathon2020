var canvas : HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context : CanvasRenderingContext2D = canvas.getContext("2d");
canvas.addEventListener("click",mouseclick,false);
canvas.addEventListener("mousemove",mousemove,false);
canvas.addEventListener("mouseup",mouseup,false);
canvas.addEventListener("mousedown",mousedown,false);
var shape : string;
var input : HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
var input3 : HTMLInputElement = <HTMLInputElement>document.getElementById("t3");
var input4 : HTMLInputElement = <HTMLInputElement>document.getElementById("t4");
var input5 : HTMLInputElement = <HTMLInputElement>document.getElementById("t5");
var change : boolean = false;
var scene : Geometry.Scene = new Geometry.Scene(canvas);

let rect = canvas.getBoundingClientRect();
function mouseclick(e : MouseEvent) {

    let p1 : Geometry.Point = new Geometry.Point(e.clientX-rect.x,e.clientY-rect.y);
    console.log(shape);

    if(!change) {
        if(shape == "circle") {
            let rad : number = +input.value;
            if(isNaN(rad)) {
                alert("Please Enter Valid Data");
                rad = 0;
            }
            let color = input3.value;
            if(rad == 0 || color == ""){
                var circle : Geometry.Circle = new Geometry.Circle(p1);
            }
            else {
                var circle : Geometry.Circle = new Geometry.Circle(p1,rad,color);
            }
            circle.draw(context);
            scene.add(circle);
        }
        else
        if(shape == "ellipse") {
            let len : number = +input5.value;
            let len1 : number = +input4.value;
            if(isNaN(len) || isNaN(len1)) {
                alert("Please Enter Valid Data");
                len = 0;
                len1 = 0;
            }
            let color = input3.value;
            if(len == 0 || color == "" || len1 == 0){
                var ell : Geometry.Ellipse = new Geometry.Ellipse(p1);
            }
            else {
                var ell : Geometry.Ellipse = new Geometry.Ellipse(p1,len,len1,color);
            }
            ell.draw(context);
            scene.add(ell);
        }
    }
    
}

function mousedown(e:MouseEvent) {
    if(change) {
        scene.draw();
    }
}

function mouseup(e : MouseEvent) {
    if(change) {
        let p1 : Geometry.Point = new Geometry.Point(e.clientX-rect.x,e.clientY-rect.y);
        scene.find(p1);
    }
}

function mousemove(e:MouseEvent) {
    if(change) {
        let p1 : Geometry.Point = new Geometry.Point(e.clientX-rect.x,e.clientY-rect.y);
        scene.find(p1);
    }
}

function circle() {
    hideInputs();
    change = false;
    shape = "circle";
    input.style.display = "";
    input3.style.display = "";
}

function ellipse() {
    hideInputs();
    change = false;
    shape = "ellipse";
    input5.style.display = "";
    input4.style.display = "";
    input3.style.display = "";
}

function hideInputs() {
    input.style.display = "none";
    input5.style.display = "none";
    input3.style.display = "none";
    input4.style.display = "none";
}

function drag() {
    hideInputs();
    change = true;
}