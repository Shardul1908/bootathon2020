namespace Geometry {
    //class point
    export class Point {
        private _x : number;
        private _y : number;

        constructor(x:number,y:number) {
            this._x = x;
            this._y = y;
        }

        get x() : number {
            return this._x;
        }

        get y() : number {
            return this._y;
        }
    }

    //class closed Path
    class ClosedPath {
        public points : Point[];
        protected color : string;

        constructor(arrayPoints:Point[],color : string = "blue") {
            this.points = arrayPoints;
            this.color = color;
        }

        //draw any generalised close path
        draw(context : CanvasRenderingContext2D) {
            context.beginPath();
            context.moveTo(this.points[0].x,this.points[0].y);
            for(let i = 0;i<this.points.length;i++) {
                context.lineTo(this.points[i].x,this.points[i].y);
            }
            context.lineTo(this.points[0].x,this.points[0].y);
            context.fillStyle = this.color;
            context.fill();
            context.strokeStyle = "Black";
            context.lineWidth = 2;
            context.stroke();
            context.closePath();   
        }

        //change color
        changeColor(color : string) {
            this.color = color
        }
    }

    //ellipse class
    export class Ellipse extends ClosedPath{
        private majorAxis : number;
        private minorAxis : number;
        private point : Point;

        constructor(point : Point,majorAxis:number = 40,minorAxis : number = 20,color : string = "blue") {
            super([],color);
            this.majorAxis = majorAxis;
            this.minorAxis = minorAxis;
            this.point = point;
        }

        //calculate pts on a ellipse
        calculate() {
            this.points = [];
            for(let i= 0;i<=360;i++){
                let x1 = this.majorAxis * Math.cos(i*(Math.PI/180)) + this.point.x;
                let y1 = this.minorAxis * Math.sin(i*(Math.PI/180)) + this.point.y;
                this.points.push(new Point(x1,y1));
            }   
        }

        draw(context : CanvasRenderingContext2D) {
            this.calculate();
            super.draw(context);
        }
    }

    //circle class
    export class Circle extends Ellipse {
        constructor(center : Point,radius : number = 20,color : string = "blue") {
            super(center,radius,radius,color);
        }
    }

    //closed Path properties
    //area isInside centroid etc
    class ClosedPathProperties {
        public centroidX : number;
        public centroidY : number;
        public area : number;
        public points : Point[];

        constructor(shape : ClosedPath) {
            this.points = shape.points;
        }

        //calculations for the properties
        private centroid() {
            var area = 0;
            var cx = 0;
            var cy = 0;
            var a = 0;

            for(let i = 0;i<this.points.length-1;i++) {
                a = (this.points[i].x * this.points[i+1].y) - 
                    (this.points[i+1].x * this.points[i].y);

                cx = cx  + (this.points[i].x + this.points[i+1].x)*a;
                cy = cy  + (this.points[i].y + this.points[i+1].y)*a;
                area = area  + a;
            }
            this.area = area/2;
            this.centroidX = cx/(6*area/2);
            this.centroidY = cy/(6*area/2);
            console.log("area = " + area);
        }

        public getCentroid() : Point{
            this.centroid();
            return new Point(this.centroidX,this.centroidY);
        }

        public getArea() {
            this.centroid();
            return this.area;
        }

        private getTraingleArea(userPoint : Point,point1 : Point,point2 : Point) {
            var area = (userPoint.x * (point1.y-point2.y) + point1.x * (point2.y-userPoint.y) + point2.x * (userPoint.y-point1.y))/2;
            return Math.abs(area);
        } 

        public isInside(point : Point) : boolean {
            var triArea = 0;
            for(let i =0;i<this.points.length-1;i++) {
                triArea += this.getTraingleArea(point,this.points[i],this.points[i+1]);
            }
            console.log("triarea" + triArea);
            console.log("areaa" + this.area);
            this.centroid();
            if(Math.abs(triArea-this.area) < 0.000001) {
                return true;
            }
            return false;
        }
    }

    //scene
    export class Scene {
        public canvas : HTMLCanvasElement;
        public geometryContainer : {goemetry : ClosedPath}[];

        constructor(canvas : HTMLCanvasElement) {
            this.canvas = canvas;
            this.geometryContainer = [];
        }
        
        add(goemetry : ClosedPath) {
            this.geometryContainer.push({goemetry : goemetry});
            this.draw();
        }

        draw() {
            var context : CanvasRenderingContext2D = this.canvas.getContext("2d");
            context.clearRect(0,0,this.canvas.width,this.canvas.height);
            for(let i = 0;i<this.geometryContainer.length;i++) {
                this.geometryContainer[i].goemetry.draw(context);
            }
        }

        //find the geometry in container
        find(point : Point,color : string = "red") {
            for(let i = 0;i<this.geometryContainer.length;i++) {
                var prop : ClosedPathProperties = new ClosedPathProperties(this.geometryContainer[i].goemetry);
                console.log(prop.isInside(point));
                if(prop.isInside(point)) {
                    console.log(prop.isInside(point));
                    this.geometryContainer[i].goemetry.changeColor(color);
                }
            }
            this.draw();
        }
    }
}


