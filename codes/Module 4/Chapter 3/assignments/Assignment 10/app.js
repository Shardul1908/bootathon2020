var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
canvas.addEventListener("click", mouseclick, false);
var shape;
var input = document.getElementById("t1");
var input3 = document.getElementById("t3");
var input4 = document.getElementById("t4");
var input5 = document.getElementById("t5");
var change = false;
var scene = new Geometry.Scene(canvas);
let rect = canvas.getBoundingClientRect();
function mouseclick(e) {
    let p1 = new Geometry.Point(e.clientX - rect.x, e.clientY - rect.y);
    console.log(shape);
    if (!change) {
        if (shape == "circle") {
            let rad = +input.value;
            if (isNaN(rad)) {
                alert("Please Enter Valid Data");
                rad = 0;
            }
            let color = input3.value;
            if (rad == 0 || color == "") {
                var circle = new Geometry.Circle(p1);
            }
            else {
                var circle = new Geometry.Circle(p1, rad, color);
            }
            circle.draw(context);
            scene.add(circle);
        }
        else if (shape == "ellipse") {
            let len = +input5.value;
            let len1 = +input4.value;
            if (isNaN(len) || isNaN(len1)) {
                alert("Please Enter Valid Data");
                len = 0;
                len1 = 0;
            }
            let color = input3.value;
            if (len == 0 || color == "" || len1 == 0) {
                var ell = new Geometry.Ellipse(p1);
            }
            else {
                var ell = new Geometry.Ellipse(p1, len, len1, color);
            }
            ell.draw(context);
            scene.add(ell);
        }
    }
    else {
        scene.find(p1);
    }
}
function circle() {
    hideInputs();
    change = false;
    shape = "circle";
    input.style.display = "";
    input3.style.display = "";
}
function ellipse() {
    hideInputs();
    change = false;
    shape = "ellipse";
    input5.style.display = "";
    input4.style.display = "";
    input3.style.display = "";
}
function hideInputs() {
    input.style.display = "none";
    input5.style.display = "none";
    input3.style.display = "none";
    input4.style.display = "none";
}
function connect() {
    hideInputs();
    change = true;
}
//# sourceMappingURL=app.js.map