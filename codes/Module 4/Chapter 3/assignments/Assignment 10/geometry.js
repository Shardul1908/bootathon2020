var Geometry;
(function (Geometry) {
    class Point {
        constructor(x, y) {
            this._x = x;
            this._y = y;
        }
        get x() {
            return this._x;
        }
        get y() {
            return this._y;
        }
    }
    Geometry.Point = Point;
    class ClosedPath {
        constructor(arrayPoints, color = "blue", point) {
            this.connect = false;
            this.points = arrayPoints;
            this.color = color;
            this.point = point;
        }
        draw(context) {
            context.beginPath();
            context.moveTo(this.points[0].x, this.points[0].y);
            for (let i = 0; i < this.points.length; i++) {
                context.lineTo(this.points[i].x, this.points[i].y);
            }
            context.lineTo(this.points[0].x, this.points[0].y);
            context.fillStyle = this.color;
            context.fill();
            context.strokeStyle = "Black";
            context.lineWidth = 2;
            context.stroke();
            context.closePath();
        }
        setConnection(pt) {
            this.connectPoint = pt;
            this.connect = true;
        }
        drawLine(context) {
            context.beginPath();
            context.moveTo(100, 100);
            context.lineTo(200, 200);
            context.strokeStyle = "black";
            context.lineWidth = 2;
            context.stroke();
            context.closePath();
        }
    }
    class Ellipse extends ClosedPath {
        constructor(point, majorAxis = 40, minorAxis = 20, color = "blue") {
            super([], color, point);
            this.majorAxis = majorAxis;
            this.minorAxis = minorAxis;
        }
        calculate() {
            this.points = [];
            for (let i = 0; i <= 360; i++) {
                let x1 = this.majorAxis * Math.cos(i * (Math.PI / 180)) + this.point.x;
                let y1 = this.minorAxis * Math.sin(i * (Math.PI / 180)) + this.point.y;
                this.points.push(new Point(x1, y1));
            }
        }
        draw(context) {
            this.calculate();
            super.draw(context);
        }
    }
    Geometry.Ellipse = Ellipse;
    class Circle extends Ellipse {
        constructor(center, radius = 20, color = "blue") {
            super(center, radius, radius, color);
        }
    }
    Geometry.Circle = Circle;
    class ClosedPathProperties {
        constructor(shape) {
            this.points = shape.points;
        }
        centroid() {
            var area = 0;
            var cx = 0;
            var cy = 0;
            var a = 0;
            for (let i = 0; i < this.points.length - 1; i++) {
                a = (this.points[i].x * this.points[i + 1].y) -
                    (this.points[i + 1].x * this.points[i].y);
                cx = cx + (this.points[i].x + this.points[i + 1].x) * a;
                cy = cy + (this.points[i].y + this.points[i + 1].y) * a;
                area = area + a;
            }
            this.area = area / 2;
            this.centroidX = cx / (6 * area / 2);
            this.centroidY = cy / (6 * area / 2);
        }
        getCentroid() {
            this.centroid();
            return new Point(this.centroidX, this.centroidY);
        }
        getArea() {
            this.centroid();
            return this.area;
        }
        getTraingleArea(userPoint, point1, point2) {
            var area = (userPoint.x * (point1.y - point2.y) + point1.x * (point2.y - userPoint.y) + point2.x * (userPoint.y - point1.y)) / 2;
            return Math.abs(area);
        }
        isInside(point) {
            var triArea = 0;
            for (let i = 0; i < this.points.length - 1; i++) {
                triArea += this.getTraingleArea(point, this.points[i], this.points[i + 1]);
            }
            this.centroid();
            if (Math.abs(triArea - this.area) < 0.000001) {
                return true;
            }
            return false;
        }
    }
    class Scene {
        constructor(canvas) {
            this.first = 0;
            this.store = -1;
            this.canvas = canvas;
            this.geometryContainer = [];
        }
        add(goemetry) {
            this.geometryContainer.push({ goemetry: goemetry });
            this.draw();
        }
        draw() {
            var context = this.canvas.getContext("2d");
            context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            for (let i = 0; i < this.geometryContainer.length; i++) {
                this.geometryContainer[i].goemetry.draw(context);
            }
        }
        find(point) {
            var context = this.canvas.getContext("2d");
            for (let i = 0; i < this.geometryContainer.length; i++) {
                var prop = new ClosedPathProperties(this.geometryContainer[i].goemetry);
                if (prop.isInside(point)) {
                    if (this.first == 0) {
                        this.store = i;
                        this.first = 1;
                        console.log("hiii" + this.store + " " + this.first);
                    }
                    else if (this.first == 1) {
                        this.geometryContainer[this.store].goemetry.setConnection(new Point(this.geometryContainer[i].goemetry.point.x, this.geometryContainer[i].goemetry.point.y));
                        if (this.geometryContainer[this.store].goemetry.connect) {
                            this.geometryContainer[this.store].goemetry.drawLine(context);
                            this.geometryContainer[this.store].goemetry.draw(context);
                            this.geometryContainer[i].goemetry.draw(context);
                            this.first = 0;
                            this.store = -1;
                            console.log("byee" + this.store + " " + this.first);
                        }
                    }
                }
            }
            this.draw();
        }
    }
    Geometry.Scene = Scene;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=geometry.js.map