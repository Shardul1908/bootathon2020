//setting up canvas
var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
//getting input
var input = document.getElementById("t1");
//mouseclick
canvas.addEventListener("click", mouseclick, false);
let rect = canvas.getBoundingClientRect();
function mouseclick(e) {
    let p1 = new Geometry.Point(e.clientX - rect.x, e.clientY - rect.y);
    input.value = p1.x + " " + p1.y; //print point clicked
}
//# sourceMappingURL=app.js.map