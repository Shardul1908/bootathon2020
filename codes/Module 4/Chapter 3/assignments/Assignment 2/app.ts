//setting up canvas
var canvas : HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context : CanvasRenderingContext2D = canvas.getContext("2d");

//getting input
var input : HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
//mouseclick
canvas.addEventListener("click",mouseclick,false);
let rect = canvas.getBoundingClientRect();
function mouseclick(e : MouseEvent) { 
        let p1 = new Geometry.Point(e.clientX-rect.x,e.clientY-rect.y);
        input.value = p1.x + " " + p1.y;   //print point clicked
}