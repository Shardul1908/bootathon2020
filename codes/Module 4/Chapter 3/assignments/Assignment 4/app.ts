//CANVAS SETTING
var canvas : HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context : CanvasRenderingContext2D = canvas.getContext("2d");
//INPUTS
var input : HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
var input2 : HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
canvas.addEventListener("click",mouseclick,false);

//MOUSECLICK EVENT
function mouseclick(e : MouseEvent) {
        var rad = +input.value;
        if(isNaN(rad)) {
                alert("Enter Valid Data");
                rad = 0;
        }
        console.log(rad);
        var color = input2.value;
        console.log(color); 
        let p1 = new Geometry.Point(e.clientX,e.clientY);
        console.log(p1.x + " " + p1.y);
        //drawing circles
        if(rad == 0 || color == ""){
                var circle : Geometry.Circle = new Geometry.Circle(p1);        
        }
        else{
                var circle : Geometry.Circle = new Geometry.Circle(p1,rad,color);
        }
        circle.draw(context);
}