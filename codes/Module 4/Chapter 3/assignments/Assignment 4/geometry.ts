namespace Geometry {
    //point class
    export class Point {
        private _x : number;
        private _y : number;

        constructor(x:number,y:number) {
            this._x = x;
            this._y = y;
        }

        //getters
        get x() {
            return this._x;
        }

        get y() {
            return this._y;
       }
    }

    //Circle
    export class Circle {
        private center : Point;
        private radius : number;
        private color : string;
        
        constructor(center : Point,radius : number = 25,color : string = "blue"){
            this.center = center;
            this.color = color;
            this.radius = radius;
        }

        //draw with context passed as parameter
        draw(context : CanvasRenderingContext2D) {
            context.beginPath();
            context.arc(this.center.x,this.center.y,this.radius,0,2*Math.PI);
            context.fillStyle = this.color;
            context.fill();
            context.strokeStyle = "black";
            context.stroke();
            context.closePath();
        }
    }
}


