var Geometry;
(function (Geometry) {
    class Point {
        constructor(x, y) {
            this._x = x;
            this._y = y;
        }
        get x() {
            return this._x;
        }
        get y() {
            return this._y;
        }
    }
    Geometry.Point = Point;
    class Circle {
        constructor(center, radius = 25, color = "blue") {
            this.center = center;
            this.color = color;
            this.radius = radius;
        }
        draw(context) {
            context.beginPath();
            context.arc(this.center.x, this.center.y, this.radius, 0, 2 * Math.PI);
            context.fillStyle = this.color;
            context.fill();
            context.strokeStyle = "black";
            context.stroke();
            context.closePath();
        }
    }
    Geometry.Circle = Circle;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=geometry.js.map