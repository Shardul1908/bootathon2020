//getting canvas
var canvas = document.getElementById("mycanvas");
//getting context
var context = canvas.getContext("2d");
//mouseclick event
canvas.addEventListener("click", mouseclick, false);
//whichimage
var image = 0;
//for getting exact coordinates of the mouse click
var rect = canvas.getBoundingClientRect();
function mouseclick(e) {
    if (image == 1) { //selected image 1
        var imag = document.getElementById("tree1");
        context.drawImage(imag, e.clientX - rect.x, e.clientY - rect.y, 200, 200);
    }
    else if (image == 2) { //selected image 2
        var imag = document.getElementById("tree2");
        context.drawImage(imag, e.clientX - rect.x, e.clientY - rect.y, 200, 200);
    }
}
//select img1
function img1() {
    image = 1;
}
//select img2
function img2() {
    image = 2;
}
//# sourceMappingURL=app.js.map