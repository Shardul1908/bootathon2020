//setting canvas
var canvas : HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context : CanvasRenderingContext2D = canvas.getContext("2d");
canvas.addEventListener("click",mouseclick,false);
var shape : string;
//inputs from users
var input : HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
var input2 : HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
var input3 : HTMLInputElement = <HTMLInputElement>document.getElementById("t3");
var input4 : HTMLInputElement = <HTMLInputElement>document.getElementById("t4");
var input5 : HTMLInputElement = <HTMLInputElement>document.getElementById("t5");


let rect = canvas.getBoundingClientRect();
function mouseclick(e : MouseEvent) {
    //mousept
    let p1 : Geometry.Point = new Geometry.Point(e.clientX-rect.x,e.clientY-rect.y);
    console.log(shape);
    console.log(p1.x + " " + p1.y);

    //draw circle
    if(shape == "circle") {
        let rad : number = +input.value;
        if(isNaN(rad)) {
            alert("Please Enter Valid Data");
            rad = 0;
        }
        let color = input3.value;
        if(rad == 0 || color == ""){
            var circle : Geometry.Circle = new Geometry.Circle(p1);
        }
        else {
            var circle : Geometry.Circle = new Geometry.Circle(p1,rad,color);
        }
        circle.draw(context);
    }
    //draw ellipse
    else
    if(shape == "ellipse") {
        let len : number = +input5.value;
        let len1 : number = +input4.value;
        if(isNaN(len) || isNaN(len1)) {
            alert("Please Enter Valid Data");
            len = 0;
            len1 = 0;
        }
        let color = input3.value;
        if(len == 0 || color == "" || len1 == 0){
            var ell : Geometry.Ellipse = new Geometry.Ellipse(p1);
        }
        else {
            var ell : Geometry.Ellipse = new Geometry.Ellipse(p1,len,len1,color);
        }
        ell.draw(context);
    }
    //draw square
    else
    if(shape == "square") {
        let len : number = +input2.value;
        if(isNaN(len)) {
            alert("Please Enter Valid Data");
            len = 0;
        }
        let color = input3.value;
        if(len == 0 || color == ""){
            var sq : Geometry.Square = new Geometry.Square(p1);
        }
        else {
            var sq : Geometry.Square = new Geometry.Square(p1,len,color);
        }
        sq.draw(context);
    }
    //draw pentagon
    else
    if(shape == "pentagon") {
        let len : number = +input2.value;
        if(isNaN(len)) {
            alert("Please Enter Valid Data");
            len = 0;
        }
        let color = input3.value;
        if(len == 0 || color == ""){
            var sq : Geometry.Pentagon = new Geometry.Pentagon(p1);
        }
        else {
            var sq : Geometry.Pentagon = new Geometry.Pentagon(p1,len,color);
        }
        sq.draw(context);
    }
    //draw hexagon
    else
    if(shape == "hexagon") {
        let len : number = +input2.value;
        if(isNaN(len)) {
            alert("Please Enter Valid Data");
            len = 0;
        }
        let color = input3.value;
        if(len == 0 || color == ""){
            var sq : Geometry.Hexagon = new Geometry.Hexagon(p1);
        }
        else {
            var sq : Geometry.Hexagon = new Geometry.Hexagon(p1,len,color);
        }
        sq.draw(context);
    }
}

//circle clicked
function circle() {
    hideInputs();
    shape = "circle";
    input.style.display = "";
    input3.style.display = "";
}

//ellipse clicked
function ellipse() {
    hideInputs();
    shape = "ellipse";
    input5.style.display = "";
    input4.style.display = "";
    input3.style.display = "";
}

//square clicked
function square() {
    hideInputs();
    shape = "square";
    input2.style.display = "";
    input3.style.display = "";
}

//pentagon clicked
function pentagon() {
    hideInputs();
    shape = "pentagon";
    input2.style.display = "";
    input3.style.display = "";
}

//hexagon clicked
function hexagon() {
    hideInputs();
    shape = "hexagon";
    input2.style.display = "";
    input3.style.display = "";
}

//hide input boxes
function hideInputs() {
    input.style.display = "none";
    input5.style.display = "none";
    input2.style.display = "none";
    input3.style.display = "none";
    input4.style.display = "none";
}