var Geometry;
(function (Geometry) {
    class Point {
        constructor(x, y) {
            this._x = x;
            this._y = y;
        }
        get x() {
            return this._x;
        }
        get y() {
            return this._y;
        }
    }
    Geometry.Point = Point;
    class ClosedPath {
        constructor(arrayPoints, color = "blue") {
            this.points = arrayPoints;
            this.color = color;
        }
        draw(context) {
            context.beginPath();
            context.moveTo(this.points[0].x, this.points[0].y);
            for (let i = 0; i < this.points.length; i++) {
                context.lineTo(this.points[i].x, this.points[i].y);
            }
            context.lineTo(this.points[0].x, this.points[0].y);
            context.fillStyle = this.color;
            context.fill();
            context.strokeStyle = "Black";
            context.lineWidth = 2;
            context.stroke();
            context.closePath();
        }
    }
    class Ellipse extends ClosedPath {
        constructor(point, majorAxis = 40, minorAxis = 20, color = "blue") {
            super([], color);
            this.majorAxis = majorAxis;
            this.minorAxis = minorAxis;
            this.point = point;
        }
        calculate() {
            this.points = [];
            for (let i = 0; i <= 360; i++) {
                let x1 = this.majorAxis * Math.cos(i * (Math.PI / 180)) + this.point.x;
                let y1 = this.minorAxis * Math.sin(i * (Math.PI / 180)) + this.point.y;
                this.points.push(new Point(x1, y1));
            }
        }
        draw(context) {
            this.calculate();
            super.draw(context);
        }
    }
    Geometry.Ellipse = Ellipse;
    class Circle extends Ellipse {
        constructor(center, radius = 20, color = "blue") {
            super(center, radius, radius, color);
        }
    }
    Geometry.Circle = Circle;
    class Polygon extends ClosedPath {
        constructor(sides, startingPoint, length = 30, color = "blue") {
            super([], color);
            this.sides = sides;
            this.startingPoint = startingPoint;
            this.length = length;
        }
        calculate() {
            this.points = [];
            var angle = 360 / this.sides;
            var ang = 0;
            for (let i = 0; i < this.sides; i++) {
                var x1 = this.startingPoint.x + this.length * Math.cos(ang * (Math.PI / 180));
                var y1 = this.startingPoint.y + this.length * Math.sin(ang * (Math.PI / 180));
                ang = ang + angle;
                this.points.push(new Point(x1, y1));
            }
        }
        draw(context) {
            this.calculate();
            super.draw(context);
        }
    }
    Geometry.Polygon = Polygon;
    class Triangle extends Polygon {
        constructor(startingPoint, length = 30, color = "blue") {
            super(3, startingPoint, length, color);
        }
    }
    Geometry.Triangle = Triangle;
    class Square extends Polygon {
        constructor(startingPoint, length = 30, color = "blue") {
            super(4, startingPoint, length, color);
        }
    }
    Geometry.Square = Square;
    class Pentagon extends Polygon {
        constructor(startingPoint, length = 30, color = "blue") {
            super(5, startingPoint, length, color);
        }
    }
    Geometry.Pentagon = Pentagon;
    class Hexagon extends Polygon {
        constructor(startingPoint, length = 30, color = "blue") {
            super(6, startingPoint, length, color);
        }
    }
    Geometry.Hexagon = Hexagon;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=geometry.js.map