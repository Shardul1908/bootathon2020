namespace Geometry {
    //class Point
    export class Point {
        private _x : number;
        private _y : number;

        constructor(x:number,y:number) {
            this._x = x;
            this._y = y;
        }

        //getters
        get x() : number {
            return this._x;
        }

        get y() : number {
            return this._y;
        }
    }

    //closed Path
    class ClosedPath {
        protected points : Point[];
        protected color : string;

        constructor(arrayPoints:Point[],color : string = "blue") {
            this.points = arrayPoints;
            this.color = color;
        }

        //generalised path drawing algorithm
        draw(context : CanvasRenderingContext2D) {
            context.beginPath();
            context.moveTo(this.points[0].x,this.points[0].y);
            for(let i = 0;i<this.points.length;i++) {
                context.lineTo(this.points[i].x,this.points[i].y);
            }
            context.lineTo(this.points[0].x,this.points[0].y);
            context.fillStyle = this.color;
            context.fill();
            context.strokeStyle = "Black";
            context.lineWidth = 2;
            context.stroke();
            context.closePath();
            
        }
    }

    //class ellipse
    export class Ellipse extends ClosedPath{
        private majorAxis : number;
        private minorAxis : number;
        private point : Point;

        constructor(point : Point,majorAxis:number = 40,minorAxis : number = 20,color : string = "blue") {
            super([],color);
            this.majorAxis = majorAxis;
            this.minorAxis = minorAxis;
            this.point = point;
        }

        //calculate pts to draw a ellipse
        calculate() {
            this.points = [];
            for(let i= 0;i<=360;i++){
                let x1 = this.majorAxis * Math.cos(i*(Math.PI/180)) + this.point.x;
                let y1 = this.minorAxis * Math.sin(i*(Math.PI/180)) + this.point.y;
                this.points.push(new Point(x1,y1));
            }   
        }

        draw(context : CanvasRenderingContext2D) {
            this.calculate();
            super.draw(context);
        }
    }

    //class circle
    export class Circle extends Ellipse {
        //drawn by calling ellipse class and setting major minor axes to equal values
        constructor(center : Point,radius : number = 20,color : string = "blue") {
            super(center,radius,radius,color);
        }
    }

    //Polygon
    export class Polygon extends ClosedPath {
        private sides : number;
        private length : number;
        private startingPoint : Point;

        constructor(sides : number , startingPoint : Point,length:number = 30,color : string = "blue") {
            super([],color);
            this.sides = sides;
            this.startingPoint = startingPoint;
            this.length = length;
        }

        //calculate points for any polygon
        calculate() {
            this.points = [];
            var angle = 360 / this.sides;
            var ang = 0;

            for(let i =0;i<this.sides;i++) {
                var x1 = this.startingPoint.x + this.length*Math.cos(ang*(Math.PI/180));
                var y1 = this.startingPoint.y + this.length*Math.sin(ang*(Math.PI/180));
                ang = ang + angle;
                this.points.push(new Point(x1,y1));
            }
        }

        draw(context : CanvasRenderingContext2D) {
            this.calculate();
            super.draw(context);
        }
    }

    export class Square extends Polygon {
        constructor(startingPoint : Point,length:number = 30,color : string = "blue") {
            super(4,startingPoint,length,color);
        }
    }

    export class Pentagon extends Polygon {
        constructor(startingPoint : Point,length:number = 30,color : string = "blue") {
            super(5,startingPoint,length,color);
        }
    }

    export class Hexagon extends Polygon {
        constructor(startingPoint : Point,length:number = 30,color : string = "blue") {
            super(6,startingPoint,length,color);
        }
    }

}


