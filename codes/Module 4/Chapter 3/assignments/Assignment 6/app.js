var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
canvas.addEventListener("click", mouseclick, false);
var shape;
var input = document.getElementById("t1");
var input2 = document.getElementById("t2");
var input3 = document.getElementById("t3");
var input4 = document.getElementById("t4");
var input5 = document.getElementById("t5");
let rect = canvas.getBoundingClientRect();
function mouseclick(e) {
    let p1 = new Geometry.Point(e.clientX - rect.x, e.clientY - rect.y);
    console.log(shape);
    console.log(p1.x + " " + p1.y);
    if (shape == "circle") {
        let rad = +input.value;
        if (isNaN(rad)) {
            alert("Please Enter Valid Data");
            rad = 0;
        }
        let color = input3.value;
        if (rad == 0 || color == "") {
            var circle = new Geometry.Circle(p1);
        }
        else {
            var circle = new Geometry.Circle(p1, rad, color);
        }
        circle.draw(context);
    }
    else if (shape == "ellipse") {
        let len = +input5.value;
        let len1 = +input4.value;
        if (isNaN(len) || isNaN(len1)) {
            alert("Please Enter Valid Data");
            len = 0;
            len1 = 0;
        }
        let color = input3.value;
        if (len == 0 || color == "" || len1 == 0) {
            var ell = new Geometry.Ellipse(p1);
        }
        else {
            var ell = new Geometry.Ellipse(p1, len, len1, color);
        }
        ell.draw(context);
    }
    else if (shape == "square") {
        let len = +input2.value;
        if (isNaN(len)) {
            alert("Please Enter Valid Data");
            len = 0;
        }
        let color = input3.value;
        if (len == 0 || color == "") {
            var sq = new Geometry.Square(p1);
        }
        else {
            var sq = new Geometry.Square(p1, len, color);
        }
        sq.draw(context);
    }
    else if (shape == "pentagon") {
        let len = +input2.value;
        if (isNaN(len)) {
            alert("Please Enter Valid Data");
            len = 0;
        }
        let color = input3.value;
        if (len == 0 || color == "") {
            var sq = new Geometry.Pentagon(p1);
        }
        else {
            var sq = new Geometry.Pentagon(p1, len, color);
        }
        sq.draw(context);
    }
    else if (shape == "hexagon") {
        let len = +input2.value;
        if (isNaN(len)) {
            alert("Please Enter Valid Data");
            len = 0;
        }
        let color = input3.value;
        if (len == 0 || color == "") {
            var sq = new Geometry.Hexagon(p1);
        }
        else {
            var sq = new Geometry.Hexagon(p1, len, color);
        }
        sq.draw(context);
    }
}
function circle() {
    hideInputs();
    shape = "circle";
    input.style.display = "";
    input3.style.display = "";
}
function ellipse() {
    hideInputs();
    shape = "ellipse";
    input5.style.display = "";
    input4.style.display = "";
    input3.style.display = "";
}
function square() {
    hideInputs();
    shape = "square";
    input2.style.display = "";
    input3.style.display = "";
}
function pentagon() {
    hideInputs();
    shape = "pentagon";
    input2.style.display = "";
    input3.style.display = "";
}
function hexagon() {
    hideInputs();
    shape = "hexagon";
    input2.style.display = "";
    input3.style.display = "";
}
function hideInputs() {
    input.style.display = "none";
    input5.style.display = "none";
    input2.style.display = "none";
    input3.style.display = "none";
    input4.style.display = "none";
}
//# sourceMappingURL=app.js.map