var Geometry;
(function (Geometry) {
    //class point
    class Point {
        constructor(x, y) {
            this._x = x;
            this._y = y;
        }
        get x() {
            return this._x;
        }
        get y() {
            return this._y;
        }
    }
    Geometry.Point = Point;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=geometry.js.map