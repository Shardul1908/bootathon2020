//setting canvas
var canvas : HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context : CanvasRenderingContext2D = canvas.getContext("2d");
var input : HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
//mousemove event
canvas.addEventListener("mousemove",mousemove,false);

//mousemove event
function mousemove(e : MouseEvent) { 
        let p1 = new Geometry.Point(e.clientX,e.clientY);
        input.value = "x = " + p1.x + " y = " + p1.y;
}