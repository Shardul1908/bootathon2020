var Geometry;
(function (Geometry) {
    class Point {
        constructor(x, y) {
            this._x = x;
            this._y = y;
        }
        get x() {
            return this._x;
        }
        get y() {
            return this._y;
        }
    }
    Geometry.Point = Point;
    class ClosedPath {
        constructor(context, arrayPoints, color = "blue") {
            this.color = "blue"; // ["red","green","yellow","blue","voilet","indigo","orange"];
            this.points = arrayPoints;
            this.context = context;
            this.color = color;
        }
        draw() {
            this.context.beginPath();
            this.context.moveTo(this.points[0].x, this.points[0].y);
            for (let i = 0; i < this.points.length; i++) {
                this.context.lineTo(this.points[i].x, this.points[i].y);
            }
            this.context.lineTo(this.points[0].x, this.points[0].y);
            this.context.fillStyle = this.color;
            this.context.fill();
            this.context.strokeStyle = "Black";
            this.context.lineWidth = 2;
            this.context.stroke();
            this.context.closePath();
        }
        random(min1, max1) {
            return (Math.random() * (max1 - min1) + min1);
        }
        changeColor() {
            this.color = "red";
        }
    }
    class Ellipse extends ClosedPath {
        constructor(context, majorAxis, minorAxis, point, color = "blue") {
            super(context, [], color);
            this.majorAxis = majorAxis;
            this.minorAxis = minorAxis;
            this.point = point;
        }
        calculate() {
            this.points = [];
            for (let i = 0; i <= 360; i++) {
                let x1 = this.majorAxis * Math.cos(i * (Math.PI / 180)) + this.point.x;
                let y1 = this.minorAxis * Math.sin(i * (Math.PI / 180)) + this.point.y;
                this.points.push(new Point(x1, y1));
            }
        }
        draw() {
            this.calculate();
            super.draw();
        }
    }
    Geometry.Ellipse = Ellipse;
    class Circle extends Ellipse {
        constructor(context, center, radius, color = "blue") {
            super(context, radius, radius, center, color);
        }
    }
    Geometry.Circle = Circle;
    class Polygon extends ClosedPath {
        constructor(context, sides, startingPoint, length, color = "blue") {
            super(context, [], color);
            this.sides = sides;
            this.startingPoint = startingPoint;
            this.length = length;
        }
        calculate() {
            this.points = [];
            var angle = 360 / this.sides;
            var ang = 0;
            for (let i = 0; i < this.sides; i++) {
                var x1 = this.startingPoint.x + this.length * Math.cos(ang * (Math.PI / 180));
                var y1 = this.startingPoint.y + this.length * Math.sin(ang * (Math.PI / 180));
                ang = ang + angle;
                this.points.push(new Point(x1, y1));
            }
        }
        draw() {
            this.calculate();
            super.draw();
        }
    }
    Geometry.Polygon = Polygon;
    class Square extends Polygon {
        constructor(context, startingPoint, length, color = "blue") {
            super(context, 4, startingPoint, length, color);
        }
    }
    Geometry.Square = Square;
    class Pentagon extends Polygon {
        constructor(context, startingPoint, length, color = "blue") {
            super(context, 5, startingPoint, length, color);
        }
    }
    Geometry.Pentagon = Pentagon;
    class Hexagon extends Polygon {
        constructor(context, startingPoint, length, color = "blue") {
            super(context, 6, startingPoint, length, color);
        }
    }
    Geometry.Hexagon = Hexagon;
    class ClosedPathProperties {
        constructor(shape) {
            this.points = shape.points;
        }
        centroid() {
            var area = 0;
            var cx = 0;
            var cy = 0;
            var a = 0;
            for (let i = 0; i < this.points.length - 1; i++) {
                a = (this.points[i].x * this.points[i + 1].y) -
                    (this.points[i + 1].x * this.points[i].y);
                cx = cx + (this.points[i].x + this.points[i + 1].x) * a;
                cy = cy + (this.points[i].y + this.points[i + 1].y) * a;
                area = area + a;
            }
            this.area = area / 2;
            this.centroidX = cx / (6 * area / 2);
            this.centroidY = cy / (6 * area / 2);
        }
        getCentroid() {
            this.centroid();
            return new Point(this.centroidX, this.centroidY);
        }
        getArea() {
            this.centroid();
            return this.area;
        }
        getTraingleArea(userPoint, point1, point2) {
            var area = (userPoint.x * (point1.y - point2.y) + point1.x * (point2.y - userPoint.y) + point2.x * (userPoint.y - point1.y)) / 2;
            return Math.abs(area);
        }
        isInside(point) {
            var triArea = 0;
            for (let i = 0; i < this.points.length - 1; i++) {
                triArea += this.getTraingleArea(point, this.points[i], this.points[i + 1]);
            }
            this.centroid();
            if (Math.abs(triArea - this.area) < 0.000001) {
                return true;
            }
            return false;
        }
    }
    Geometry.ClosedPathProperties = ClosedPathProperties;
    class Scene {
        constructor(canvas) {
            this.canvas = canvas;
            this.geometryContainer = [];
        }
        add(goemetry) {
            this.geometryContainer.push({ goemetry: goemetry });
            this.draw();
        }
        draw() {
            var context = this.canvas.getContext("2d");
            context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            for (let i = 0; i < this.geometryContainer.length; i++) {
                this.geometryContainer[i].goemetry.draw();
            }
        }
        find(point) {
            for (let i = 0; i < this.geometryContainer.length; i++) {
                var prop = new ClosedPathProperties(this.geometryContainer[i].goemetry);
                if (prop.isInside(point)) {
                    this.geometryContainer[i].goemetry.changeColor();
                }
            }
            this.draw();
        }
    }
    Geometry.Scene = Scene;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=geometry.js.map