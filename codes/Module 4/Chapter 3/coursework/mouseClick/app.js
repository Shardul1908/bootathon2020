var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
var input = document.getElementById("t1");
var input1 = document.getElementById("t2");
canvas.addEventListener("click", mouseclick, false);
canvas.addEventListener("mousemove", mousemove, false);
canvas.addEventListener("mouseup", mouseup, false);
canvas.addEventListener("mousedown", mousedown, false);
var changeColor = false;
var scene = new Geometry.Scene(canvas);
function mouseclick(e) {
    if (changeColor) {
        let p1 = new Geometry.Point(e.clientX, e.clientY);
        input.value = p1.x + " " + p1.y;
        scene.find(p1);
    }
    else {
        let p1 = new Geometry.Point(e.clientX, e.clientY);
        input.value = p1.x + " " + p1.y;
        let circle = new Geometry.Circle(context, p1, 25);
        scene.add(circle);
        scene.draw();
    }
}
function mousedown(e) {
}
function mouseup(e) {
    scene.draw();
}
function mousemove(e) {
    let p1 = new Geometry.Point(e.clientX, e.clientY);
    input1.value = p1.x + " " + p1.y;
    scene.draw();
}
function changeColour() {
    changeColor = true;
}
//# sourceMappingURL=app.js.map