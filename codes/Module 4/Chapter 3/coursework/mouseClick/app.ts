var canvas : HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context : CanvasRenderingContext2D = canvas.getContext("2d");
var input : HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
var input1 : HTMLInputElement = <HTMLInputElement>document.getElementById("t2");

canvas.addEventListener("click",mouseclick,false);
canvas.addEventListener("mousemove",mousemove,false);
canvas.addEventListener("mouseup",mouseup,false);
canvas.addEventListener("mousedown",mousedown,false);
var changeColor : boolean = false;

var scene : Geometry.Scene = new Geometry.Scene(canvas);

function mouseclick(e : MouseEvent) {
    if(changeColor) {
        let p1 = new Geometry.Point(e.clientX,e.clientY);
        input.value = p1.x + " " + p1.y;
        scene.find(p1);
    }  
    else{
        let p1 = new Geometry.Point(e.clientX,e.clientY);
        input.value = p1.x + " " + p1.y;
        let circle : Geometry.Circle = new Geometry.Circle(context,p1,25);
        scene.add(circle);
        scene.draw();    
    }
}

function mousedown(e:MouseEvent) {

}

function mouseup(e : MouseEvent) {
    scene.draw();
}

function mousemove(e : MouseEvent) {
    let p1 = new Geometry.Point(e.clientX,e.clientY);
    input1.value = p1.x + " " + p1.y;
    scene.draw();
}

function changeColour() {
    changeColor = true;
}