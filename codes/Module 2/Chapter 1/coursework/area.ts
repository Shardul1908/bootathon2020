function area(){
    var x1s : HTMLInputElement = <HTMLInputElement>document.getElementById("x1");
    var y1s : HTMLInputElement = <HTMLInputElement>document.getElementById("y1");
    var x2s : HTMLInputElement = <HTMLInputElement>document.getElementById("x2");
    var y2s : HTMLInputElement = <HTMLInputElement>document.getElementById("y2");
    var x3s : HTMLInputElement = <HTMLInputElement>document.getElementById("x3");
    var y3s : HTMLInputElement = <HTMLInputElement>document.getElementById("y3");

    var x1:number = parseFloat(x1s.value);
    var x2:number = parseFloat(x2s.value);
    var x3:number = parseFloat(x3s.value);
    var y1:number = parseFloat(y1s.value);
    var y2:number = parseFloat(y2s.value);
    var y3:number = parseFloat(y3s.value);

    var a:number = Math.sqrt(Math.pow((x2-x1),2)+Math.pow((y2-y1),2));
    var b:number = Math.sqrt(Math.pow((x1-x3),2)+Math.pow((y1-y3),2));
    var c:number = Math.sqrt(Math.pow((x3-x2),2)+Math.pow((y3-y2),2));

    var s : number=(a+b+c)/2;
    console.log(s);

    var area : number = Math.sqrt((s)*(s-a)*(s-b)*(s-c));
    console.log(area);
    
    var ans : HTMLInputElement = <HTMLInputElement>document.getElementById("ans");
    ans.value = area.toString();
}