function area1() {
    var rad : HTMLInputElement = <HTMLInputElement>document.getElementById("rad");
    var ans : HTMLInputElement = <HTMLInputElement>document.getElementById("ans");
    var radius : number = parseFloat(rad.value);

    var ar = (Math.PI)*radius*radius;

    ans.value = ar.toString();
}