function value1() : void {
    //getting input
    var nums : HTMLInputElement = <HTMLInputElement>document.getElementById("num");

    //Parsing the data
    var num: number = +nums.value;
    
    //Validating the data
    if(isNaN(num)){
        alert("Please Enter valid Data");
    }
    else{                                   //Correct data
        var ans : number;
        var cos : number = Math.cos(num*(Math.PI/180));
        ans = num+cos;                                                          //Calculations
        var answer : HTMLInputElement = <HTMLInputElement>document.getElementById("val");
        answer.value = ans.toString();                                            //Printing answer
    }
}