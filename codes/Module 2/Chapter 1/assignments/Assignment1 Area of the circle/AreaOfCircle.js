function area() {
    //Getting data from the user
    var rad = document.getElementById("rad");
    var ans = document.getElementById("ans");
    //Parsing the data
    var radius = parseFloat(rad.value);
    //Validating the data
    if (isNaN(radius)) {
        alert("Please Enter Valid Data");
    }
    else { //correct data
        var ar = (Math.PI) * radius * radius; //calculating the area
        ans.value = ar.toString(); //Printing the Area in the text box
    }
}
//# sourceMappingURL=AreaOfCircle.js.map