function area() {
    //Getting data from the user
    var rad : HTMLInputElement = <HTMLInputElement>document.getElementById("rad");
    var ans : HTMLInputElement = <HTMLInputElement>document.getElementById("ans");
    
    //Parsing the data
    var radius : number = parseFloat(rad.value);

    //Validating the data
    if(isNaN(radius)) {
        alert("Please Enter Valid Data");
    }
    else {                              //correct data
        var ar = (Math.PI)*radius*radius;               //calculating the area
        ans.value = ar.toString();                      //Printing the Area in the text box
    }
}