function check() {
    var a = document.getElementById("t1");
    var data = +a.value;
    if (isNaN(data)) {
        alert("NOT A NUMBER");
    }
    else {
        document.getElementById("display").innerHTML = data.toString();
    }
}
function minmax() {
    var x1 = document.getElementById("x");
    var y1 = document.getElementById("y");
    var x = +x1.value;
    var y = +y1.value;
    if (x < y) {
        alert("x is lesser than y");
    }
    else if (x == y) {
        alert("x is equal to y");
    }
    else if (x > y) {
        alert("x is greater than y");
    }
}
function type1() {
    alert("hi");
    var ss1 = document.getElementById("s1");
    var ss2 = document.getElementById("s2");
    var ss3 = document.getElementById("s3");
    var s1 = +ss1.value;
    var s2 = +ss2.value;
    var s3 = +ss3.value;
    if ((s1 == s2) && (s2 == s3)) {
        document.getElementById("display").innerHTML = "The triangle is a equilateral triangle";
    }
    else if ((s1 == s2) && (s1 != s3)) {
        document.getElementById("display").innerHTML = "The triangle is a isosceles triangle";
    }
    else {
        document.getElementById("display").innerHTML = "The triangle is a scalene triangle";
    }
}
function complex() {
    var complex = document.getElementById("c");
    var real = document.getElementById("c1");
    var img = document.getElementById("c2");
    var data = complex.value;
    var realnum;
    var imgnum;
    var i = data.indexOf("+");
    if (i != -1) {
        realnum = parseFloat(data.substring(0, i));
        imgnum = parseFloat(data.substring(i + 1, data.length));
        real.value = realnum.toString();
        img.value = imgnum.toString();
    }
}
//# sourceMappingURL=decision.js.map