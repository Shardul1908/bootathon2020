function check1() : void {
    //Getting Input from the user
    var x1s : HTMLInputElement = <HTMLInputElement>document.getElementById("x1");
    var y1s : HTMLInputElement = <HTMLInputElement>document.getElementById("y1");
    var x2s : HTMLInputElement = <HTMLInputElement>document.getElementById("x2");
    var y2s : HTMLInputElement = <HTMLInputElement>document.getElementById("y2");
    var x3s : HTMLInputElement = <HTMLInputElement>document.getElementById("x3");
    var y3s : HTMLInputElement = <HTMLInputElement>document.getElementById("y3");
    var xs : HTMLInputElement = <HTMLInputElement>document.getElementById("x");
    var ys : HTMLInputElement = <HTMLInputElement>document.getElementById("y");


    //converting into numbers
    var x1:number = parseFloat(x1s.value);
    var x2:number = parseFloat(x2s.value);
    var x3:number = parseFloat(x3s.value);
    var y1:number = parseFloat(y1s.value);
    var y2:number = parseFloat(y2s.value);
    var y3:number = parseFloat(y3s.value);
    var x:number = parseFloat(xs.value);
    var y:number = parseFloat(ys.value);

    //Validating the data entered by the user
    if(isNaN(x1) || isNaN(x2) || isNaN(x3) || isNaN(y1) || isNaN(y2) || isNaN(y3)) {
        alert("Please Enter Valid Data");
    }
    else {    //correct data entered
        var mainArea : number = area(x1,y1,x2,y2,x3,y3);                            //area of Main traingle
        var area1 : number = area(x,y,x2,y2,x3,y3);
        var area2 : number = area(x1,y2,x,y,x3,y3);                                  //areas of the traingles when vertices are joined to the point given
        var area3 : number = area(x1,y1,x2,y2,x,y);

        var area4 = area1 + area2 + area3;                                          //Sum of the three traingles hence formed
        mainArea = parseInt(mainArea.toString());
        area4 = parseInt(area4.toString());
        var diff:number = mainArea - area4;                                              //substraction
        diff = parseInt(diff.toString());
        if(diff == 0) {                                      //If the point is inside the traingles hence formed will have sum equal to the area of the main traingle
            document.getElementById("ans").innerHTML = "The given Point is in the traingle";
        }
        else{
            document.getElementById("ans").innerHTML = "The given Point is not in the traingle";
        }
    }
}
function area(x1:number,y1:number,x2:number,y2:number,x3:number,y3:number) : number {
        var a:number = Math.sqrt(Math.pow((x2-x1),2)+Math.pow((y2-y1),2));       
        var b:number = Math.sqrt(Math.pow((x1-x3),2)+Math.pow((y1-y3),2));              //Calculations for length of the sides
        var c:number = Math.sqrt(Math.pow((x3-x2),2)+Math.pow((y3-y2),2));

        var s : number=(a+b+c)/2;           //Calculating s

        var area : number = Math.sqrt((s)*(s-a)*(s-b)*(s-c));           //Calculating area
        area = Math.abs(area);                                              //Taking Absolute becouse cannot be zero
        return area;
    } 