function check1() {
    //Getting Input from the user
    var x1s = document.getElementById("x1");
    var y1s = document.getElementById("y1");
    var x2s = document.getElementById("x2");
    var y2s = document.getElementById("y2");
    var x3s = document.getElementById("x3");
    var y3s = document.getElementById("y3");
    var xs = document.getElementById("x");
    var ys = document.getElementById("y");
    //converting into numbers
    var x1 = parseFloat(x1s.value);
    var x2 = parseFloat(x2s.value);
    var x3 = parseFloat(x3s.value);
    var y1 = parseFloat(y1s.value);
    var y2 = parseFloat(y2s.value);
    var y3 = parseFloat(y3s.value);
    var x = parseFloat(xs.value);
    var y = parseFloat(ys.value);
    //Validating the data entered by the user
    if (isNaN(x1) || isNaN(x2) || isNaN(x3) || isNaN(y1) || isNaN(y2) || isNaN(y3)) {
        alert("Please Enter Valid Data");
    }
    else { //correct data entered
        var mainArea = area(x1, y1, x2, y2, x3, y3); //area of Main traingle
        var area1 = area(x, y, x2, y2, x3, y3);
        var area2 = area(x1, y2, x, y, x3, y3); //areas of the traingles when vertices are joined to the point given
        var area3 = area(x1, y1, x2, y2, x, y);
        var area4 = area1 + area2 + area3; //Sum of the three traingles hence formed
        mainArea = parseInt(mainArea.toString());
        area4 = parseInt(area4.toString());
        var diff = mainArea - area4; //substraction
        diff = parseInt(diff.toString());
        if (diff == 0) { //If the point is inside the traingles hence formed will have sum equal to the area of the main traingle
            document.getElementById("ans").innerHTML = "The given Point is in the traingle";
        }
        else {
            document.getElementById("ans").innerHTML = "The given Point is not in the traingle";
        }
    }
}
function area(x1, y1, x2, y2, x3, y3) {
    var a = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
    var b = Math.sqrt(Math.pow((x1 - x3), 2) + Math.pow((y1 - y3), 2)); //Calculations for length of the sides
    var c = Math.sqrt(Math.pow((x3 - x2), 2) + Math.pow((y3 - y2), 2));
    var s = (a + b + c) / 2; //Calculating s
    var area = Math.sqrt((s) * (s - a) * (s - b) * (s - c)); //Calculating area
    area = Math.abs(area); //Taking Absolute becouse cannot be zero
    return area;
}
//# sourceMappingURL=inside.js.map