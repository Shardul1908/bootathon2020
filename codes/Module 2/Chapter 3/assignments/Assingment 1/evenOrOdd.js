function check1() {
    //getting data from user
    var nums = document.getElementById("num");
    //parsing data
    var num = +nums.value;
    //validating the data
    if (isNaN(num)) {
        alert("Please enter Valid data");
    }
    else { //correcting the data
        if (num < 0) {
            num = Math.abs(num); //if number is negetive taking absolute value
        }
        if (num % 2 == 0) {
            alert("The Entered number is Even"); //even number answer
        }
        else {
            alert("The Entered number is Odd"); //odd number answer
        }
    }
}
//# sourceMappingURL=evenOrOdd.js.map