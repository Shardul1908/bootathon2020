function check1() {
    //Getting Input from the user
    var side1s = document.getElementById("s1");
    var side2s = document.getElementById("s2");
    var side3s = document.getElementById("s3");
    //converting into numbers
    var side1 = parseFloat(side1s.value);
    var side2 = parseFloat(side2s.value);
    var side3 = parseFloat(side3s.value);
    //Validating the data entered by the user
    if (isNaN(side1) || isNaN(side2) || isNaN(side3)) {
        alert("Please Enter Valid Data");
    }
    else { //correct data
        if (side1 == side2 && side2 == side3) { //conditions for equilateral triangle
            document.getElementById("display").innerHTML = "The triangle is a Equilateral Traingle.<br>";
        }
        else if ((side1 == side2 && side1 != side3) || (side2 == side3 && side2 != side1) || (side1 == side3 && side1 != side2)) { //conditions for isosceles triangle
            document.getElementById("display").innerHTML = "The triangle is a Isosceles Traingle.<br>";
        }
        else { //conditions for scalene triangle
            document.getElementById("display").innerHTML = "The triangle is a Scalene Traingle.<br>";
        }
        if ((Math.pow(side1, 2) + Math.pow(side2, 2) == Math.pow(side3, 2)) ||
            (Math.pow(side2, 2) + Math.pow(side3, 2) == Math.pow(side1, 2)) ||
            (Math.pow(side1, 2) + Math.pow(side3, 2) == Math.pow(side2, 2))) { //conditions for right angled traingle using pythagoras theorem
            document.getElementById("display").innerHTML += "The triangle is a also a Right angled Traingle.<br>";
        }
    }
}
//# sourceMappingURL=TypeTriangle.js.map