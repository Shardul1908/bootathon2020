function Split() {
    //getting input
    var str = document.getElementById("complex");
    //parsing into string variable
    var strr = str.value;
    //position of +
    var pos = strr.indexOf("+");
    console.log(pos);
    var realIsNegetive = false; //Taking two booleans to check whether parts are negetive or not
    var imgIsNegetive = false;
    if (pos == -1) { //The imaginary part is negative
        imgIsNegetive = true;
        pos = strr.indexOf("-");
        if (pos == 0) { //both img and real part are negative     
            realIsNegetive = true;
            strr = strr.substring(pos + 1, strr.length); //deleting - sign of real part from the string so that we get the position of the img - sign to split the string
            console.log(strr);
            pos = strr.indexOf("-");
        }
    }
    if (realIsNegetive) { //real is negative
        document.getElementById("real").innerHTML += "-" + strr.substring(0, pos);
    }
    else { //real is positive
        document.getElementById("real").innerHTML += strr.substring(0, pos);
    }
    if (imgIsNegetive) { //img is negative
        document.getElementById("img").innerHTML += "-" + strr.substring(pos + 1, strr.length - 1);
    }
    else { //img is positive
        document.getElementById("img").innerHTML += strr.substring(pos + 1, strr.length - 1);
    }
    console.log(pos);
}
//# sourceMappingURL=complex.js.map