function getNumbers(): void {
    //getting data
    var nums : HTMLInputElement = <HTMLInputElement>document.getElementById("num");
    //Parsing data
    var num : number = +nums.value;

    var neg : number = 0;
    var pos : number = 0;                                           //Initialinzing counters to zero
    var zeros : number = 0;
    if(isNaN(num)) {                    //validating the data
        alert("Please Enter Valid Data");
    }
    else{                                           //correct data
        var i : number = num;
        while(i>0) {
            var str : string = prompt("Enter the number : ");                       //taking input
            var entNum : number = +str;
            if(isNaN(entNum)) {                             //validating input
                alert("Please Enter Valid Data");
            }                               
            else{                                               //correct input
                if(entNum < 0) {                                
                    neg++;                                      //input is negative
                }
                else if(entNum > 0){
                    pos++;                                      //input is positive
                }
                else{
                    zeros++;                                    //input is zero
                }
            }
            i--;
        }
        document.getElementById("neg").innerHTML += neg.toString();
        document.getElementById("pos").innerHTML += pos.toString();                                 //Printing answers
        document.getElementById("zero").innerHTML += zeros.toString();
    }
}