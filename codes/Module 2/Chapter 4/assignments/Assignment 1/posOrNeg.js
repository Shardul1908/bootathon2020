function getNumbers() {
    //getting data
    var nums = document.getElementById("num");
    //Parsing data
    var num = +nums.value;
    var neg = 0;
    var pos = 0; //Initialinzing counters to zero
    var zeros = 0;
    if (isNaN(num)) { //validating the data
        alert("Please Enter Valid Data");
    }
    else { //correct data
        var i = num;
        while (i > 0) {
            var str = prompt("Enter the number : "); //taking input
            var entNum = +str;
            if (isNaN(entNum)) { //validating input
                alert("Please Enter Valid Data");
            }
            else { //correct input
                if (entNum < 0) {
                    neg++; //input is negative
                }
                else if (entNum > 0) {
                    pos++; //input is positive
                }
                else {
                    zeros++; //input is zero
                }
            }
            i--;
        }
        document.getElementById("neg").innerHTML += neg.toString();
        document.getElementById("pos").innerHTML += pos.toString(); //Printing answers
        document.getElementById("zero").innerHTML += zeros.toString();
    }
}
//# sourceMappingURL=posOrNeg.js.map