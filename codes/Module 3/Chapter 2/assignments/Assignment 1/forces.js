var forces = [];
var tab = document.getElementById("tab");
var table = new Table(tab);
var num1;
function getData(str) {
    var nums = document.getElementById(str);
    var num = +nums.value;
    if (isNaN(num)) { //Validating data
        alert("Please Enter valid data");
        return 0;
    }
    return num;
}
//This funtion creates the table
function table1() {
    //Getting the number of numbers from user
    var select = document.getElementById("noOfForces");
    num1 = +select.value.toString();
    var count;
    table.clearIt(); //erasing the table till it has only headers left
    for (count = 1; count <= num1; count++) { //running for num times
        var row = table.addRow(); //creating a row
        table.addCell(row, "t", count);
        table.addCell(row, "tt", count);
    }
}
//get The forces from the table
function getForces() {
    var select = document.getElementById("noOfForces");
    num1 = +select.value.toString();
    var count;
    //traversing table text boxes
    for (count = 1; count <= num1; count++) {
        var mag = document.getElementById("t" + count);
        var dir = document.getElementById("tt" + count);
        forces.push({ magnitude: +mag.value, direction: +dir.value });
        if (isNaN(forces[count - 1].magnitude) || isNaN(forces[count - 1].direction)) { //validating data
            alert("Enter Valid Data");
        }
    }
}
//Gets the sum of the forces in x or y directions
function sum1(getType) {
    var sums = 0;
    if (getType == 1) { //calculate summation fx
        for (let i = 0; i < forces.length; i++) {
            var fcos = forces[i].magnitude * Math.cos(forces[i].direction * (Math.PI / 180));
            sums += fcos;
        }
    }
    else if (getType == 2) { //calculate summation fy
        for (let i = 0; i < forces.length; i++) {
            var fsin = forces[i].magnitude * Math.sin(forces[i].direction * (Math.PI / 180));
            sums += fsin;
        }
    }
    return sums;
}
//calculates resultant
function calculate() {
    getForces();
    var fx = sum1(1); //1 for summation fx
    var fy = sum1(2); //2 for summation fy
    var resmag = Math.sqrt(Math.pow(fx, 2) + Math.pow(fy, 2));
    var resDir = (180 / Math.PI) * Math.atan2(fy, fx);
    document.getElementById("ans").innerHTML = "The magnitude of the resultant = " + resmag + "<br>The direction of the resultant is = " + resDir;
}
//# sourceMappingURL=forces.js.map