var row1;
var col1;
var row2;
var col2;
var tab = document.getElementById("matrix1");
var table1 = new Table(tab);
tab = document.getElementById("matrix2");
var table2 = new Table(tab);
tab = document.getElementById("product");
var table3 = new Table(tab);
var mat1 = [];
var mat2 = [];
var mat3 = [];
function multiply() {
    mat1 = LoadMatrix(row1, col1, "a");
    mat2 = LoadMatrix(row2, col2, "b");
    mat3 = mult(mat1, mat2, row1, col1, row2, col2);
    printResult();
}
function getData(str) {
    return getSelectData(str);
}
function Matrix1() {
    row1 = getData("rows1");
    col1 = getData("cols1");
    console.log(row1);
    console.log(col1);
    makeMatrixTable(table1, "a", row1, col1);
}
function Matrix2() {
    row2 = getData("rows2");
    col2 = getData("cols2");
    console.log(row2);
    console.log(col2);
    makeMatrixTable(table2, "b", row2, col2);
}
function LoadMatrix(r, c, str) {
    var matrix = [];
    for (let i = 0; i < r; i++) {
        matrix[i] = [];
        for (let j = 0; j < c; j++) {
            var nums = document.getElementById(str + i + j);
            var num = +nums.value;
            if (isNaN(num)) {
                alert("Enter valid data");
                num = 0;
            }
            matrix[i][j] = num;
        }
    }
    return matrix;
}
function mult(matrix1, matrix2, r1, c1, r2, c2) {
    var proc = [];
    if (c1 == r2) {
        for (let i = 0; i < r1; i++) {
            proc[i] = [];
            for (let j = 0; j < c2; j++) {
                proc[i][j] = 0;
            }
        }
        for (let i = 0; i < r1; i++) {
            for (let j = 0; j < c2; j++) {
                for (let k = 0; k < r2; k++) {
                    proc[i][j] += matrix1[i][k] * matrix2[k][j];
                }
            }
        }
        return proc;
    }
    else {
        alert("Sorry the matrices cannot be multiplied");
    }
}
function printResult() {
    row1 = getData("rows1");
    col2 = getData("cols2");
    console.log(row1);
    console.log(col2);
    makeMatrixTable(table3, "c", row1, col2);
    for (let i = 0; i < row1; i++) {
        for (let j = 0; j < row2; j++) {
            var inp = document.getElementById("c" + i + j);
            inp.value = mat3[i][j].toString();
        }
    }
}
//getting data
function getSelectData(str) {
    var select = document.getElementById(str);
    return +select.value.toString();
}
function makeMatrixTable(table, id, row, col) {
    table.clearIt();
    for (let i = 0; i < row; i++) {
        var roww = table.addRow();
        for (let j = 0; j < col; j++) {
            table.addCell(roww, id, i, j);
        }
    }
}
//# sourceMappingURL=app.js.map