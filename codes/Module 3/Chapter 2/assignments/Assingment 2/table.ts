//class table
class Table {
    private table : HTMLTableElement;
    
    constructor(table : HTMLTableElement) {
        this.table = table;
    }

    clearIt() {                 //removing all rows
        while(this.table.rows.length  > 1) {
            this.table.deleteRow(1);
        }
    }

    addRow() : HTMLTableRowElement {            //Adding a new Row
        return this.table.insertRow();
    }

    addCell(row : HTMLTableRowElement,str : string,num:number,num1 : number){
        var cell : HTMLTableDataCellElement = row.insertCell();             //insert cell in row
        var text : HTMLInputElement = document.createElement("input");          //creating element 
        text.type = "text";
        text.style.textAlign = "center";                        //aligning text to centre
        text.id = str + num + num1;                       //assigning id to the text boxes
        cell.appendChild(text);
    }
}