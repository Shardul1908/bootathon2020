//number of row and columns for both the matrices
var row1 : number;
var col1 : number;
var row2 : number;
var col2 : number;

//Tables for the Input and Answer
var tab : HTMLTableElement = <HTMLTableElement>document.getElementById("matrix1");
var table1 = new Table(tab);
tab =  <HTMLTableElement>document.getElementById("matrix2");
var table2 = new Table(tab);
tab =  <HTMLTableElement>document.getElementById("product");
var table3 = new Table(tab);

//Declaring matrices
var mat1 : number[][] = [];
var mat2 : number[][] = [];
var mat3 : number[][] = [];

//gets data from the user and multiplies the matrices 
function multiply() {
    mat1 = LoadMatrix(row1,col1,"a");
    mat2 = LoadMatrix(row2,col2,"b");
    mat3 = mult(mat1,mat2,row1,col1,row2,col2);
    printResult();
}

//Gets data from select tags
function getData(str : string) : number {
    return getSelectData(str); 
}
//creates table of matrix1
function Matrix1() {
    row1 = getData("rows1");
    col1 = getData("cols1");
    console.log(row1);
    console.log(col1);
    makeMatrixTable(table1,"a",row1,col1);
}

//creates table of matrix2
function Matrix2() {
    row2 = getData("rows2");
    col2 = getData("cols2");
    console.log(row2);
    console.log(col2);
    makeMatrixTable(table2,"b",row2,col2);
}

//Gets data from the tables
function LoadMatrix(r:number,c:number,str : string) : number[][] {
    var matrix : number[][] = [];
    for(let i =0;i<r;i++) {
        matrix[i] = [];
        for(let j=0;j<c;j++) {
            var nums : HTMLInputElement = <HTMLInputElement>document.getElementById(str + i + j);
            var num : number = +nums.value;
            if(isNaN(num)) {
                alert("Enter valid data");
                num = 0;
            }
            matrix[i][j] = num;
        }
    }
    return matrix;
}

//Multiply algorithm of the matrices
function mult(matrix1 : number[][],matrix2 : number[][],r1 : number,c1 : number,r2:number,c2:number) : number[][] {
    var proc : number[][] = []; 
    if(c1 == r2) {              //checking if the 2 matrices can be multiplied or not
        for(let i =0;i<r1;i++) {
            proc[i] = [];
            for(let j=0;j<c2;j++) {
                proc[i][j] = 0;
            }
        }
        for(let i =0;i<r1;i++) {
            for(let j=0;j<c2;j++) {
                for(let k =0;k<r2;k++) {
                    proc[i][j] += matrix1[i][k]*matrix2[k][j]
                }
            }
        }
        return proc;
    }
    else {
        alert("Sorry the matrices cannot be multiplied");
    }
}

//Creates table for the Answer Matrix and prints the contents as well
function printResult() {
    row1 = getData("rows1");
    col2 = getData("cols2");
    console.log(row1);
    console.log(col2);
    makeMatrixTable(table3,"c",row1,col2);
    for(let i =0;i<row1;i++) {
        for(let j=0;j<col2;j++) {
            var inp : HTMLInputElement = <HTMLInputElement>document.getElementById("c" + i + j)
            inp.value = mat3[i][j].toString();
        }
    }
}

//getting data
function getSelectData(str : string) {
    var select : HTMLSelectElement = <HTMLSelectElement>document.getElementById(str);
    return +select.value.toString();
}

//Makes table for all the matrices
function makeMatrixTable(table: Table,id : string,row: number,col: number) {
    table.clearIt();
    for(let i =0;i<row;i++) {
        var roww : HTMLTableRowElement = table.addRow();
        for(let j=0;j<col;j++) {
            table.addCell(roww,id,i,j);
        }
    }
}