class Table {
    constructor(table) {
        this.table = table;
    }
    clearIt() {
        while (this.table.rows.length > 1) {
            this.table.deleteRow(1);
        }
    }
    addRow() {
        return this.table.insertRow();
    }
    addCell(row, str, num, num1) {
        var cell = row.insertCell(); //insert cell in row
        var text = document.createElement("input"); //creating element 
        text.type = "text";
        text.style.textAlign = "center"; //aligning text to centre
        text.id = str + num + num1; //assigning id to the text boxes
        cell.appendChild(text);
    }
}
//# sourceMappingURL=table.js.map