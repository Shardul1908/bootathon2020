//setting up the canvas
var canvas : HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context : CanvasRenderingContext2D = canvas.getContext("2d");

//drawing the dial
var circle : Circle = new Circle(350,350,250,context);
var dial : Dial = new Dial(circle,context);
dial.draw();
//initially it will point at 0
dial.pointAt(0);

//after button pressed with correct value the dial will change position
function change() {
    var nums : HTMLInputElement = <HTMLInputElement>document.getElementById("num");
    var num : number = +nums.value;
    if(isNaN(num)) {
        alert("Please Enter Valid Data");
        num = 0;
    }
    context.clearRect(0,0,canvas.width,canvas.height);
    dial.draw();
    dial.pointAt(num);
}