//class dial
class Dial {
    private circle : Circle;
    private centrex : number;
    private centrey : number;
    private radius : number;
    private context : CanvasRenderingContext2D;
    private meter : Line; 

    //setting up data
    constructor(circle : Circle ,context : CanvasRenderingContext2D) {
        this.circle = circle;
        this.centrex = circle.getCentreX();
        this.centrey = circle.getCentreY();
        this.radius = circle.getRadius();
        this.context = context;
        this.meter = new Line(this.centrex,this.centrey,this.centrex+this.radius-50,this.centrey,context);
    }

    //draw the dial
    draw() {
        this.circle.draw();
        for(let i = 0;i<360;i++) {
            this.context.beginPath();
            this.context.moveTo(this.centrex+((this.radius-10)*Math.cos(i*(Math.PI/180))),this.centrey+((this.radius-10)*Math.sin(i*(Math.PI/180))));
            this.context.lineTo(this.centrex+(this.radius*Math.cos(i*(Math.PI/180))),this.centrey+(this.radius*Math.sin(i*(Math.PI/180))));
            this.context.strokeStyle = "black"
            this.context.stroke();
        }
    }

    //point the meter at the specified number
    pointAt(loc : number) {
        while(loc > 360) {
            loc = loc - 360;
        }
        this.context.beginPath();
        this.context.moveTo(this.centrex,this.centrey);
        this.context.lineTo(this.centrex+((this.radius-50)*Math.cos(-loc*(Math.PI/180))),this.centrey+((this.radius-50)*Math.sin(-loc*(Math.PI/180))));
        this.context.strokeStyle = "black"
        this.context.stroke();
    }
}