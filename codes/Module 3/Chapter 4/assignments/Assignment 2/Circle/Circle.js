class Circle {
    constructor(centrex, centrey, radius, context) {
        this.centrex = centrex;
        this.centrey = centrey;
        this.radius = radius;
        this.context = context;
        this.startAngle = 0;
        this.endAngle = 2 * Math.PI;
        this.direction = false;
    }
    draw() {
        this.context.beginPath();
        this.context.arc(this.centrex, this.centrey, this.radius, this.startAngle, this.endAngle, this.direction);
        this.context.strokeStyle = "black";
        this.context.lineWidth = 2;
        this.context.fillStyle = "wheat";
        this.context.fill();
        this.context.stroke();
    }
    getCentreX() {
        return this.centrex;
    }
    getCentreY() {
        return this.centrey;
    }
    getRadius() {
        return this.radius;
    }
}
//# sourceMappingURL=Circle.js.map