//class Circle
class Circle {
    private centrex : number;
    private centrey : number;
    private radius : number;
    private context : CanvasRenderingContext2D;
    private startAngle : number;
    private endAngle : number;
    private direction : boolean;

    //Setting the data
    constructor(centrex: number,centrey: number,radius: number,context : CanvasRenderingContext2D) {
        this.centrex = centrex;
        this.centrey = centrey;
        this.radius = radius;
        this.context = context;
        this.startAngle = 0;
        this.endAngle = 2*Math.PI;
        this.direction = false;
    }

    //draw the circle
    draw() {
        this.context.beginPath();
        this.context.arc(this.centrex,this.centrey,this.radius,this.startAngle,this.endAngle,this.direction);
        this.context.strokeStyle = "black";
        this.context.lineWidth = 2;
        this.context.fillStyle = "wheat";
        this.context.fill();
        this.context.stroke();

    }

    //getters for the center coordinates and radius
    getCentreX() : number {
        return this.centrex;
    }

    getCentreY() : number {
        return this.centrey;
    }

    getRadius() : number {
        return this.radius;
    }
}