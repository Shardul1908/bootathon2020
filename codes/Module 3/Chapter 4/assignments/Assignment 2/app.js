var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
var circle = new Circle(350, 350, 250, context);
var dial = new Dial(circle, context);
dial.draw();
dial.pointAt(0);
function change() {
    var nums = document.getElementById("num");
    var num = +nums.value;
    if (isNaN(num)) {
        alert("Please Enter Valid Data");
        num = 0;
    }
    context.clearRect(0, 0, canvas.width, canvas.height);
    dial.draw();
    dial.pointAt(num);
}
//# sourceMappingURL=app.js.map