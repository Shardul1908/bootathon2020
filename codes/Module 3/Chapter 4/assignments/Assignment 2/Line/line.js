class Line {
    constructor(x1, y1, x2, y2, context) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.context = context;
    }
    draw() {
        this.context.beginPath();
        this.context.moveTo(this.x1, this.y1);
        this.context.lineTo(this.x2, this.y2);
        this.context.strokeStyle = "black";
        this.context.lineWidth = 2;
        this.context.stroke();
    }
    setX1(num) {
        this.x1 = num;
    }
    setX2(num) {
        this.x2 = num;
    }
    setY1(num) {
        this.y1 = num;
    }
    setY2(num) {
        this.y2 = num;
    }
}
//# sourceMappingURL=line.js.map