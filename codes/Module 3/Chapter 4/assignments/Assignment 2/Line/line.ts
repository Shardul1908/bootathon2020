class Line {
    private x1 : number;
    private y1 : number;
    private x2 : number;
    private y2 : number;
    private context : CanvasRenderingContext2D;

    constructor(x1 : number,y1 : number,x2 : number ,y2 : number,context: CanvasRenderingContext2D) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.context = context;
    }

    draw() {
        this.context.beginPath();
        this.context.moveTo(this.x1,this.y1);
        this.context.lineTo(this.x2,this.y2);
        this.context.strokeStyle = "black";
        this.context.lineWidth = 2;
        this.context.stroke();
    }

    setX1(num : number) {
        this.x1 = num;
    }

    setX2(num : number) {
        this.x2 = num;
    }

    setY1(num : number) {
        this.y1 = num;
    }

    setY2(num : number) {
        this.y2 = num;
    }
}