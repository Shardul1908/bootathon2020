//class pendulum
class Pendulum {
    private bob : Circle;
    private thread : Line;
    private frame : Line;

    //setting data
    constructor(bob : Circle,thread : Line,frame : Line) {
        this.bob = bob;
        this.thread = thread;
        this.frame = frame;
    }

    //drawing the pendulum
    draw() {
        this.bob.draw();
        this.thread.draw();
        this.frame.draw();
    }
}