class Pendulum {
    constructor(bob, thread, frame) {
        this.bob = bob;
        this.thread = thread;
        this.frame = frame;
    }
    draw() {
        this.bob.draw();
        this.thread.draw();
        this.frame.draw();
    }
}
//# sourceMappingURL=pendulum.js.map