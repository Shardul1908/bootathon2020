//Setting data
var canvas : HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context : CanvasRenderingContext2D = canvas.getContext("2d");

//Gathering attributes
var circle : Circle = new Circle(300,300,30,context);
var line : Line = new Line(300,270,300,170,context);
var line1 : Line = new Line(250,170,350,170,context);

//Pendulum Object
var pendulum : Pendulum = new Pendulum(circle,line,line1);
pendulum.draw();
//draw Pendulum