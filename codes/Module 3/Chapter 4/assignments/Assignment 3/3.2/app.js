var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
var circle = new Circle(300, 300, 30, context);
var line = new Line(300, 270, 300, 170, context);
var line1 = new Line(250, 170, 350, 170, context);
var pendulum = new Pendulum(circle, line, line1);
pendulum.draw();
//# sourceMappingURL=app.js.map