class CosCurve {
    constructor(centrex, centrey, amplitude, context) {
        this.centrex = centrex;
        this.centrey = centrey;
        this.amplitude = amplitude;
        this.context = context;
    }
    draw() {
        this.context.beginPath();
        this.context.fillText("Cos Curve - Red", this.centrex + 100, this.centrey - 90);
        this.context.moveTo(this.centrex, this.centrey);
        this.context.lineTo(this.centrex, this.centrey - 100);
        this.context.moveTo(this.centrex, this.centrey);
        this.context.lineTo(this.centrex, this.centrey + 100);
        this.context.moveTo(this.centrex, this.centrey);
        this.context.lineTo(this.centrex + 500, this.centrey);
        this.context.moveTo(this.centrex, this.centrey);
        this.context.lineTo(this.centrex - 100, this.centrey);
        this.context.lineWidth = 2;
        this.context.strokeStyle = "black";
        this.context.stroke();
        var x = this.centrex;
        var y = this.centrey;
        this.context.beginPath();
        this.context.moveTo(x, y + this.amplitude * Math.cos(Math.PI / 180));
        for (let i = 0; i < 360; i++) {
            this.context.lineTo(x + i, y + this.amplitude * Math.cos(i * (Math.PI / 180)));
        }
        this.context.lineWidth = 3;
        this.context.strokeStyle = "red";
        this.context.stroke();
    }
}
//# sourceMappingURL=CosCurve.js.map