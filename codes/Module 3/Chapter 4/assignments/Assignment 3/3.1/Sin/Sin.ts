//class Sine
class SinCurve { 
    private centrex : number;
    private centrey : number;
    private amplitude : number;
    private context : CanvasRenderingContext2D;

    //setting up data
    constructor(centrex : number,
                centrey : number,
                amplitude : number,
                context : CanvasRenderingContext2D) {
                    this.centrex = centrex;
                    this.centrey = centrey;
                    this.amplitude = amplitude;
                    this.context = context;
                }

    //draw the curve            
    draw() {
        //axis
        this.context.beginPath();
        this.context.fillText("Sin Curve - blue",this.centrex+100,this.centrey-100);
        this.context.moveTo(this.centrex,this.centrey);
        this.context.lineTo(this.centrex,this.centrey-100);
        this.context.moveTo(this.centrex,this.centrey);
        this.context.lineTo(this.centrex,this.centrey+100);
        this.context.moveTo(this.centrex,this.centrey);
        this.context.lineTo(this.centrex+500,this.centrey);
        this.context.moveTo(this.centrex,this.centrey);
        this.context.lineTo(this.centrex-100,this.centrey);
        this.context.lineWidth = 2;
        this.context.strokeStyle = "black";
        this.context.stroke();

        //drawing the curve
        var x : number = this.centrex;
        var y : number = this.centrey;
        this.context.beginPath();
        this.context.moveTo(x,y + this.amplitude*Math.sin(Math.PI/180));
        for(let i = 0;i<360;i++) {
            this.context.lineTo(x+i,y+this.amplitude*Math.sin(i*(Math.PI/180)));
        }
        this.context.lineWidth = 3;
        this.context.strokeStyle = "blue";
        this.context.stroke();
    }
}