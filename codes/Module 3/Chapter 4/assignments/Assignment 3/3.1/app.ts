//setting up the canvas
var canvas : HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context : CanvasRenderingContext2D = canvas.getContext("2d");

//SinCurve
var sin : SinCurve = new SinCurve(200,200,75,context);
sin.draw();

//CosCurve
var cos : CosCurve = new CosCurve(200,200,75,context);
cos.draw();
