//setting up the canvas
var canvas : HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context : CanvasRenderingContext2D = canvas.getContext("2d");

//Collecting all the attributes of the andGate
var semi : SemiCircle = new SemiCircle(200,200,30,context,Math.PI/2,3*Math.PI/2,true);
var line : Line = new Line(200,170,200,230,context);
var input1 : Line = new Line(150,185,200,185,context);
var input2 : Line = new Line(150,215,200,215,context);
var output : Line = new Line(230,200,280,200,context);

//Passing above variables to draw a andGate
var and : AndGate = new AndGate(semi,line,input1,input2,output);
and.draw();


//collecting all the attributes of the notGate
var triangle : Traingle = new Traingle(400,400,350,450,350,350,context);
var input : Line = new Line(350,400,300,400,context);
var out : Line = new Line(400,400,450,400,context);

//Passing above data to draw NotGate
var not : NotGate = new NotGate(triangle,input,out);
not.draw();
