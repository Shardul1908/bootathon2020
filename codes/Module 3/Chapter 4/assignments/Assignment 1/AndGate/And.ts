//And Gate class
class AndGate {
    private semi : SemiCircle;
    private line : Line;
    private input1 : Line;
    private input2 : Line;
    private output : Line;
    
    //setting attributes
    constructor(semi : SemiCircle,line : Line,input1 : Line,input2 : Line,output : Line) {
        this.semi = semi;
        this.line = line;
        this.input1 = input1;
        this.input2 = input2;
        this.output = output;
    }

    //Draw the gate
    draw() {
        this.semi.draw();
        this.line.draw();
        this.input1.draw();
        this.input2.draw();
        this.output.draw();
    }
}