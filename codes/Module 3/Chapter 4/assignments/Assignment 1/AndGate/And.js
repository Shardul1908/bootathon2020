class AndGate {
    constructor(semi, line, input1, input2, output) {
        this.semi = semi;
        this.line = line;
        this.input1 = input1;
        this.input2 = input2;
        this.output = output;
    }
    draw() {
        this.semi.draw();
        this.line.draw();
        this.input1.draw();
        this.input2.draw();
        this.output.draw();
    }
}
//# sourceMappingURL=And.js.map