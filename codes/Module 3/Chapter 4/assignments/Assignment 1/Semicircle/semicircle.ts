//class semicircle
class SemiCircle {
    private centrex : number;
    private centrey : number;
    private radius : number;
    private context : CanvasRenderingContext2D;
    private startAngle : number;
    private endAngle : number;
    private direction : boolean;

    //seeting Attributes
    constructor(centrex: number,centrey: number,radius: number,context : CanvasRenderingContext2D,
        startAngle : number,endAngle : number ,direction :boolean) {
        this.centrex = centrex;
        this.centrey = centrey;
        this.radius = radius;
        this.context = context;
        this.startAngle = startAngle;
        this.endAngle = endAngle;
        this.direction = direction;
    }

    //draw the semicircle
    draw() {
        this.context.beginPath();
        this.context.arc(this.centrex,this.centrey,this.radius,this.startAngle,this.endAngle,this.direction);
        this.context.strokeStyle = "black";
        this.context.lineWidth = 2;
        this.context.fillStyle = "gray";
        this.context.fill();
        this.context.stroke();
    }
}