class SemiCircle {
    constructor(centrex, centrey, radius, context, startAngle, endAngle, direction) {
        this.centrex = centrex;
        this.centrey = centrey;
        this.radius = radius;
        this.context = context;
        this.startAngle = startAngle;
        this.endAngle = endAngle;
        this.direction = direction;
    }
    draw() {
        this.context.beginPath();
        this.context.arc(this.centrex, this.centrey, this.radius, this.startAngle, this.endAngle, this.direction);
        this.context.strokeStyle = "black";
        this.context.lineWidth = 2;
        this.context.fillStyle = "gray";
        this.context.fill();
        this.context.stroke();
    }
}
//# sourceMappingURL=semicircle.js.map