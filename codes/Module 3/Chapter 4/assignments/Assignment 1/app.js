var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
var semi = new SemiCircle(200, 200, 30, context, Math.PI / 2, 3 * Math.PI / 2, true);
var line = new Line(200, 170, 200, 230, context);
var input1 = new Line(150, 185, 200, 185, context);
var input2 = new Line(150, 215, 200, 215, context);
var output = new Line(230, 200, 280, 200, context);
var and = new AndGate(semi, line, input1, input2, output);
and.draw();
var triangle = new Traingle(400, 400, 350, 450, 350, 350, context);
var input = new Line(350, 400, 300, 400, context);
var out = new Line(400, 400, 450, 400, context);
var not = new NotGate(triangle, input, out);
not.draw();
//# sourceMappingURL=app.js.map