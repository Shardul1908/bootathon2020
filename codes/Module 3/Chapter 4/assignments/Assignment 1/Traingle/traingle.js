class Traingle {
    constructor(x1, y1, x2, y2, x3, y3, context) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.x3 = x3;
        this.y3 = y3;
        this.context = context;
    }
    draw() {
        this.context.beginPath();
        this.context.moveTo(this.x1, this.y1);
        this.context.lineTo(this.x2, this.y2);
        this.context.lineTo(this.x3, this.y3);
        this.context.lineTo(this.x1, this.y1);
        this.context.strokeStyle = "black";
        this.context.lineWidth = 2;
        this.context.fillStyle = "gray";
        this.context.fill();
        this.context.stroke();
    }
}
//# sourceMappingURL=traingle.js.map