//class traingle
class Traingle {
    private x1 : number;
    private y1 : number;
    private x2 : number;
    private y2 : number;
    private x3 : number;
    private y3 : number;
    private context : CanvasRenderingContext2D;

    //setting data
    constructor(x1:number,y1:number,x2:number,y2:number,x3:number,y3:number,context:CanvasRenderingContext2D) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.x3 = x3;
        this.y3 = y3;
        this.context = context;
    }

    //draw the traingle
    draw() {
        this.context.beginPath();
        this.context.moveTo(this.x1,this.y1);
        this.context.lineTo(this.x2,this.y2);
        this.context.lineTo(this.x3,this.y3);
        this.context.lineTo(this.x1,this.y1);
        this.context.strokeStyle = "black";
        this.context.lineWidth = 2;
        this.context.fillStyle = "gray";
        this.context.fill();
        this.context.stroke();
    }
}