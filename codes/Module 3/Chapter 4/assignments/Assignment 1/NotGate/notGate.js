class NotGate {
    constructor(triangle, input, output) {
        this.triangle = triangle;
        this.input = input;
        this.output = output;
    }
    draw() {
        this.triangle.draw();
        this.input.draw();
        this.output.draw();
    }
}
//# sourceMappingURL=notGate.js.map