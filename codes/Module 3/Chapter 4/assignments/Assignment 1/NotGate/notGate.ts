//Class Not Gate
class NotGate {
    private triangle : Traingle;
    private input : Line;
    private output : Line;

    //setting data
    constructor(triangle : Traingle,input : Line,output : Line) {
        this.triangle = triangle;
        this.input = input;
        this.output = output;
    }

    //Draw the not Gate
    draw() {
        this.triangle.draw();
        this.input.draw();
        this.output.draw();
    }
}