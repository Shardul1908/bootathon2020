class Table {
    private table : HTMLTableElement;
    
    constructor(table : HTMLTableElement) {
        this.table = table;
    }

    clearIt() {
        while(this.table.rows.length  > 1) {
            this.table.deleteRow(1);
        }
    }

    addRow() : HTMLTableRowElement {
        return this.table.insertRow();
    }

    addCell(row : HTMLTableRowElement,str:string){
        var cell : HTMLTableDataCellElement = row.insertCell();             //insert cell in row
        var text : HTMLInputElement = document.createElement("input");          //creating element 
        text.type = "text";
        text.style.textAlign = "center";                        //aligning text to centre
        text.value = str;                      //assigning id to the text boxes
        cell.appendChild(text);
    }
}