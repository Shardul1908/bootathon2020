let list1 : HTMLSelectElement = <HTMLSelectElement>document.getElementById("fruitlist");
let fruits : {value:number,name:string,type:string,price:number}[] = [];
var tab : HTMLTableElement = <HTMLTableElement>document.getElementById("tab");
var table = new Table(tab);
var i :number = 1;
var sum : number = 0;
fruits.push({value:0,name:"Mango",type:"Regular",price:500});
fruits.push({value:1,name:"Apple",type:"Regular",price:100});
fruits.push({value:2,name:"Orange",type:"Regular",price:400});
fruits.push({value:3,name:"Banana",type:"Regular",price:300});
fruits.push({value:4,name:"Watermelon",type:"Regular",price:200});
fruits.push({value:5,name:"Grapes",type:"Regular",price:50});
fruits.push({value:6,name:"Mango",type:"High",price:800});
fruits.push({value:7,name:"Apple",type:"High",price:300});
fruits.push({value:8,name:"Orange",type:"High",price:600});
fruits.push({value:9,name:"Banana",type:"High",price:500});
fruits.push({value:10,name:"Grapes",type:"High",price:100});

for(let i =0;i<fruits.length;i++) {
    let option :HTMLOptionElement = <HTMLOptionElement>document.createElement("option");
    option.text = fruits[i].name + "--" + fruits[i].type;
    option.value = fruits[i].value.toString();
    list1.add(option);
}

function put(){
    let qty1:HTMLInputElement = <HTMLInputElement>document.getElementById("qty");
    let qty : number = +qty1.value;
    if(isNaN(qty)) {
        alert("Enter Valid Data");
    }
    let selected : HTMLInputElement = <HTMLInputElement>document.getElementById("fruitlist");
    for(let i =0;i<fruits.length;i++) {
        if(fruits[i].value.toString() == selected.value) {
            var price = fruits[i].price;
            var name = fruits[i].name;
            var type = fruits[i].type;
            var value = fruits[i].value;
        }
    }

    var row = table.addRow();              //creating a row

    table.addCell(row,i.toString());            //creating cells and the data to the table using function in table class
    table.addCell(row,name);
    table.addCell(row,type);
    table.addCell(row,price.toString());
    table.addCell(row,qty.toString());
    i++;

    var total : number = price*qty;
    sum += total;
    
    document.getElementById("Total").innerHTML = "Total price = " + sum.toString(); 
}
