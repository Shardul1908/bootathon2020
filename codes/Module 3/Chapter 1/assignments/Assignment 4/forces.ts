var forces : {magnitude:number,direction:number}[] = [];
var tab : HTMLTableElement = <HTMLTableElement>document.getElementById("tab");
var table = new Table(tab);
var num1 : number;
function getData(str :string) : number{
    var nums : HTMLInputElement = <HTMLInputElement>document.getElementById(str);
    var num : number = +nums.value;
    if(isNaN(num)) {    //Validating data
        alert("Please Enter valid data");
        return 0;
    }
    return num;
}

//This funtion creates the table
function table1():void {
    //Getting the number of numbers from user
    var select : HTMLSelectElement = <HTMLSelectElement>document.getElementById("noOfForces");
    num1 = +select.value.toString();
    var count : number;
    table.clearIt();                            //erasing the table till it has only headers left
    for(count = 1;count<=num1;count++) {                             //running for num times
        var row = table.addRow();              //creating a row
        table.addCell(row,"t",count);
        table.addCell(row,"tt",count);
    }
}

//get The forces from the table
function getForces() : void {
    var select : HTMLSelectElement = <HTMLSelectElement>document.getElementById("noOfForces");
    num1 = +select.value.toString();
    var count : number;
    //traversing table text boxes
    for(count = 1;count<=num1;count++){
        var mag : HTMLInputElement = <HTMLInputElement>document.getElementById("t" + count);
        var dir : HTMLInputElement = <HTMLInputElement>document.getElementById("tt" + count);
        forces.push({magnitude: +mag.value, direction : +dir.value});        
        if(isNaN(forces[count-1].magnitude) || isNaN(forces[count-1].direction)){           //validating data
            alert("Enter Valid Data");
        }
    }
}

//Gets the sum of the forces in x or y directions
function sum1(getType:number) : number {
    var sums:number = 0;
    console.log(getType);
    if(getType == 1) {  
        console.log("hi");                    //calculate summation fx
        for(let i = 0;i<forces.length;i++) {
            var fcos : number = forces[i].magnitude*Math.cos(forces[i].direction*(Math.PI/180));
            console.log("fcos = " + fcos);
            sums += fcos;
        }
    }
    else if(getType == 2) {
        console.log("bye")                      //calculate summation fy
        for(let i = 0;i<forces.length;i++) {
            var fsin : number = forces[i].magnitude*Math.sin(forces[i].direction*(Math.PI/180));
            console.log("fsin = " + fsin);
            sums += fsin;
        }
    }
    console.log("sums = " +sums);
    return sums;
}

//calculates resultant
function calculate() : void {
    getForces();
    var fx : number = sum1(1);              //1 for summation fx
    var fy : number = sum1(2);              //2 for summation fy
    var resmag : number = Math.sqrt(Math.pow(fx,2)+Math.pow(fy,2));
    var resDir : number = (180/Math.PI)*Math.atan2(fy,fx);
    document.getElementById("ans").innerHTML = "The magnitude of the resultant = " + resmag + "<br>The direction of the resultant is = " + resDir;
}
