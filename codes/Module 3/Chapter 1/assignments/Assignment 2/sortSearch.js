var arr = [];
var tab = document.getElementById("tab");
var table = new Table(tab);
var num;
function getData(str) {
    var nums = document.getElementById(str);
    var number = +nums.value;
    if (isNaN(number)) { //Validating data
        alert("Please Enter valid data");
        return 0;
    }
    return number;
}
//This funtion creates the table
function table2() {
    //Getting the number of numbers from user
    num = getData("num");
    var count;
    table.clearIt(); //erasing the table till it has only headers left
    for (count = 1; count <= num; count++) { //running for num times
        var row = table.addRow(); //creating a row
        table.addCell(row, "t", count);
        table.addCell(row, "tt", count);
    }
}
function sort1() {
    num = getData("num");
    for (let count = 1; count <= num; count++) {
        var strr = "t" + count;
        var no = getData(strr);
        arr[count - 1] = no;
    }
    for (let i = 0; i < arr.length - 1; i++) { //sorting the array using bubble sort
        for (let j = i + 1; j < arr.length; j++) {
            if (arr[i] > arr[j]) {
                var temp;
                temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
    }
    for (let count = 1; count <= num; count++) { //printing th elements if cube array
        var nos = document.getElementById("tt" + count);
        nos.value = arr[count - 1].toString();
    }
}
function search1() {
    num = getData("num");
    // var searchs : HTMLInputElement = <HTMLInputElement>document.getElementById("search");
    // var search : number = +searchs.value;
    // if(isNaN(search)) {    //Validating data
    //     alert("Please Enter valid data");
    // }
    var search = getData("search");
    for (let count = 1; count <= num; count++) {
        var strr = "t" + count;
        var no = getData(strr);
        arr[count - 1] = no;
    }
    var flag = false; //boolean field for searching
    for (let i = 0; i < arr.length; i++) { //traversing the array to find the element
        if (search == arr[i]) //if found flag becomes true
         {
            flag = true;
            break;
        }
    }
    if (flag) { //found element
        document.getElementById("display").innerHTML = "Element Found in the Array";
    }
    else { //not found element
        document.getElementById("display").innerHTML = "Element Not Found in the Array";
    }
}
//# sourceMappingURL=sortSearch.js.map