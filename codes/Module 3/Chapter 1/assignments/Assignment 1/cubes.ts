var tab : HTMLTableElement = <HTMLTableElement>document.getElementById("tab");
var table = new Table(tab);
var input : number;

function tablee() {
    input = getNum("num");
    var count : number;
    table.clearIt();

    for(count = 1;count<=input;count++) {
        var row = table.addRow();
        table.addCell(row,"t",count);
        table.addCell(row,"tt",count);
    }
}

//gets the cubes of the numbers
function cube():void {
    //Getting the number of numbers from user
    input = getNum("num");     
    var arr : number[] = [];
    var cubes : number[] = [];
    for(let count = 1;count<=input;count++){ 
        let str : string = "t" + count;
        var no = getNum(str);
        arr[count-1] = no;
    }

    for(let i = 0;i<arr.length;i++) {                               //cubing the elements of the arr to putting them into cubes array
        cubes[i] = arr[i]*arr[i]*arr[i];
    }

    for(let count = 1;count<=input;count++){                             //printing th elements if cube array
        var nos : HTMLInputElement = <HTMLInputElement>document.getElementById("tt"+count);
        nos.value = cubes[count-1].toString();
    }
}

function getNum(str : string) : number {
    var nums : HTMLInputElement = <HTMLInputElement>document.getElementById(str);
    var num : number;
    num = +nums.value;
    if(isNaN(num)) {    //Validating data
        alert("Please Enter valid data");
        return 0;
    }
    return num;
}