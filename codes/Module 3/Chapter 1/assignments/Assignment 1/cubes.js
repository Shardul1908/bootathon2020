//table from the html page
var tab = document.getElementById("tab");
//creating a object of table class
var table = new Table(tab);
var input;
function tablee() {
    input = getNum("num");
    var count;
    table.clearIt();
    for (count = 1; count <= input; count++) {
        var row = table.addRow();
        table.addCell(row, "t", count);
        table.addCell(row, "tt", count);
    }
}
//gets the cubes of the numbers
function cube() {
    //Getting the number of numbers from user
    input = getNum("num");
    var arr = [];
    var cubes = [];
    for (let count = 1; count <= input; count++) {
        let str = "t" + count;
        var no = getNum(str);
        arr[count - 1] = no;
    }
    for (let i = 0; i < arr.length; i++) { //cubing the elements of the arr to putting them into cubes array
        cubes[i] = arr[i] * arr[i] * arr[i];
    }
    for (let count = 1; count <= input; count++) { //printing th elements if cube array
        var nos = document.getElementById("tt" + count);
        nos.value = cubes[count - 1].toString();
    }
}
function getNum(str) {
    var nums = document.getElementById(str);
    var num;
    num = +nums.value;
    if (isNaN(num)) { //Validating data
        alert("Please Enter valid data");
    }
    return num;
}
//# sourceMappingURL=cubes.js.map