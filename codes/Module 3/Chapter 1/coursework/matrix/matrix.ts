var mat1 : number[][] = [[10],[10],[10]];
var mat2 : number[][] = [[10],[10],[10]];
var mat3 : number[][] = [[10],[10],[10]];

var row : number = parseInt(prompt("Enter the number of rows: "));
var col : number = parseInt(prompt("Enter the number of columns: "));

for(let i =0;i<row;i++) {
    for(let j =0;j<col;j++) {
        mat1[i][j] = parseInt(prompt("Enter the mat1 element for row = " + i + " column = " + j));
    }
}

for(let i =0;i<row;i++) {
    for(let j =0;j<col;j++) {
        mat2[i][j] = parseInt(prompt("Enter the mat2 element for row = " + i + " column = " + j));
    }
}


for(let i =0;i<row;i++) {
    for(let j =0;j<col;j++) {
        mat3[i][j] = mat1[i][j] + mat2[i][j];
    }
}

for(let i =0;i<row;i++) {
    for(let j =0;j<col;j++) {
        document.getElementById("first").innerHTML += mat1[i][j] + "  ";
        document.getElementById("second").innerHTML += mat2[i][j] + "  ";
        document.getElementById("ans").innerHTML += mat3[i][j] + "  ";
    }
    document.getElementById("first").innerHTML += "<br>";
    document.getElementById("second").innerHTML += "<br>";
    document.getElementById("ans").innerHTML += "<br>";
}