declare var drawgraph;              //frameWork

var dataPoints: {x:number,y:number}[] = [];             //datapoints

for(let i = 0;i<=100;i++) {                         //pushing data
    dataPoints.push({x:i,y:i*i});
}

drawgraph("g1",dataPoints,"X-axis","Y-axis");                   //using framework

declare var drawgraph2;             //framework

var dataPoints2: {x:number,y:number}[] = [];                    //datapoints

for(let i = 0;i<=25;i++) {                         //pushing data
    dataPoints2.push({x:i,y:i*i*i});
}

var dataPoints3: {x:number,y:number}[] = [];             //datapoints

for(let i = 0;i<=25;i++) {                         //pushing data
    dataPoints3.push({x:i,y:(i*i*i+3*i*i+2*i+4)});
}

drawgraph2("g2",dataPoints2,dataPoints3,"X-axis","Y-axis","Comparision","x^3","x^3+3x^2+2x+4");


declare var graphline;                                                      //framework
var dataPoints4: {x:number,y:number}[] = [];

//Pushing square points
dataPoints4.push({x:10,y:10});
dataPoints4.push({x:50,y:10});
dataPoints4.push({x:50,y:50});
dataPoints4.push({x:10,y:50});
dataPoints4.push({x:10,y:10});

graphline("g3",dataPoints4,"X-axis","Y-axis");


declare var graphlogx;

var dataPoints5 : {x:number,y:number}[] = [];

//pushing random data
dataPoints5.push({x:10,y:10});
dataPoints5.push({x:17,y:123});
dataPoints5.push({x:33,y:535});
dataPoints5.push({x:45,y:1352});
dataPoints5.push({x:55,y:2345});
dataPoints5.push({x:67,y:5647});
dataPoints5.push({x:78,y:8817});
dataPoints5.push({x:83,y:10023});
dataPoints5.push({x:92,y:15096});
dataPoints5.push({x:99,y:30005});
dataPoints5.push({x:110,y:50000});

graphlogx("g4",dataPoints5,"X-axis","Y-axis");

