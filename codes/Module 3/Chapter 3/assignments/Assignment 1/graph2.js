var dataPoints5 = [];
dataPoints5.push({ x: 10, y: 10 });
dataPoints5.push({ x: 17, y: 123 });
dataPoints5.push({ x: 33, y: 535 });
dataPoints5.push({ x: 45, y: 1352 });
dataPoints5.push({ x: 55, y: 2345 });
dataPoints5.push({ x: 67, y: 5647 });
dataPoints5.push({ x: 78, y: 8817 });
dataPoints5.push({ x: 83, y: 10023 });
dataPoints5.push({ x: 92, y: 15096 });
dataPoints5.push({ x: 99, y: 30005 });
dataPoints5.push({ x: 110, y: 50000 });
graphlogy("g1", dataPoints5, "X-axis", "Y-axis");
var dataPoints6 = [];
dataPoints6.push({ x: 10, y: 10 });
dataPoints6.push({ x: 12, y: 173 });
dataPoints6.push({ x: 25, y: 537 });
dataPoints6.push({ x: 37, y: 1423 });
dataPoints6.push({ x: 47, y: 2376 });
dataPoints6.push({ x: 59, y: 5847 });
dataPoints6.push({ x: 63, y: 7837 });
dataPoints6.push({ x: 78, y: 11023 });
dataPoints6.push({ x: 84, y: 16096 });
dataPoints6.push({ x: 99, y: 28005 });
dataPoints6.push({ x: 110, y: 47000 });
var data = [];
data.push({ type: "line", dataPoints: dataPoints5 });
data.push({ type: "line", dataPoints: dataPoints6 });
logymultiple("Loggraph", "X axis", "Y-axis", data, "g2");
var dataPoints7 = [];
var dataPoints8 = [];
dataPoints7.push({ x: 10, y: 10 });
dataPoints7.push({ x: 50, y: 10 });
dataPoints7.push({ x: 50, y: 50 });
dataPoints7.push({ x: 10, y: 50 });
dataPoints7.push({ x: 10, y: 10 });
dataPoints8.push({ x: 10, y: 10 });
dataPoints8.push({ x: 20, y: 20 });
dataPoints8.push({ x: 30, y: 30 });
dataPoints8.push({ x: 40, y: 40 });
dataPoints8.push({ x: 50, y: 50 });
dataPoints8.push({ x: 60, y: 60 });
dataPoints8.push({ x: 70, y: 70 });
dataPoints8.push({ x: 80, y: 80 });
dataPoints8.push({ x: 90, y: 90 });
dataPoints8.push({ x: 100, y: 100 });
var data = [];
data.push({
    type: "spline",
    xValueType: "Float",
    showInLegend: false,
    name: "f(x)",
    markerSize: 2,
    dataPoints: dataPoints7
});
data.push({
    type: "line",
    xValueType: "Float",
    showInLegend: true,
    name: "f(x1)",
    markerSize: 2,
    dataPoints: dataPoints8
});
graphline("g3", data, "", "", 100, 0);
//# sourceMappingURL=graph2.js.map