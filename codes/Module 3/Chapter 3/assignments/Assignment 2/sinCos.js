var datapoints = [];
var datapoints1 = [];
var amp = 15 * Math.PI;
for (let i = 0; i <= amp; i++) {
    var sinValue = Math.sin(i);
    var cosValue = Math.cos(i);
    datapoints.push({ x: i, y: sinValue });
    datapoints1.push({ x: i, y: cosValue });
}
drawgraph("g1", datapoints, "X-axis", "Y-axis");
drawgraph("g2", datapoints1, "X-axis", "Y-axis");
//# sourceMappingURL=sinCos.js.map