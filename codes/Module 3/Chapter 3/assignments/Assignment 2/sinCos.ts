//framework
declare var drawgraph;
//data array for sin
var datapoints : {x:number,y:number}[] = [];
//data array for cos
var datapoints1 : {x:number,y:number}[] = [];
//amplitude
var amp : number = 15*Math.PI;

//pushing data
for(let i = 0;i<=amp;i++) {
    var sinValue : number = Math.sin(i);
    var cosValue : number = Math.cos(i);
    datapoints.push({x:i,y:sinValue});
    datapoints1.push({x:i,y:cosValue});
}

//drawing graphs
drawgraph("g1",datapoints,"X-axis","Y-axis");
drawgraph("g2",datapoints1,"X-axis","Y-axis");
